/* See LICENSE file for license and copyright information */
#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif
#if defined(_XOPEN_SOURCE) && _XOPEN_SOURCE < 500
# undef _XOPEN_SOURCE
#endif
#ifndef _XOPEN_SOURCE
# define _XOPEN_SOURCE  500
#endif

#ifndef _DEFAULT_SOURCE
# define _DEFAULT_SOURCE
#endif

#include <libgen.h>
#include <limits.h>
#include <math.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

#include <poppler/glib/poppler.h>
#include <cairo.h>

#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>


/* macros */
#define NORETURN   __attribute__((noreturn))
#define LENGTH(x)  (sizeof(x)/sizeof((x)[0]))
#define CLEAN(m)   (m&~(GDK_MOD5_MASK|GDK_MOD2_MASK|GDK_BUTTON1_MASK|GDK_BUTTON2_MASK|GDK_BUTTON3_MASK|GDK_BUTTON4_MASK|GDK_BUTTON5_MASK|GDK_LEAVE_NOTIFY_MASK))


// just remove the #if-#define-#endif block if support for poppler versions
// before 0.15 is dropped
#if !POPPLER_CHECK_VERSION(0,15,0)
# define poppler_page_get_selected_text poppler_page_get_text
#endif

static const char *main_group_name = "//main//";
static const char *lastcheck_key_name = "lastcheck";


//**************************************************************************
//
// surface cache
//
//**************************************************************************

// maximum surfaces in page surface cache
#define SURF_CACHE_SIZE  (16)

typedef struct PageSurfCache_t {
  cairo_surface_t *surf;
  int page_id;
  double scale;
  GList *search_results;
  struct PageSurfCache_t *prev;
  struct PageSurfCache_t *next;
} PageSurfCache;


PageSurfCache *surfCacheHead = NULL;
PageSurfCache *surfCacheTail = NULL;


//==========================================================================
//
//  surfCacheInit
//
//  initialise page cache, if necessary
//
//==========================================================================
static void surfCacheInit (void) {
  if (surfCacheHead) return;
  surfCacheHead = (PageSurfCache *)g_malloc0(sizeof(PageSurfCache));
  memset(surfCacheHead, 0, sizeof(PageSurfCache));
  surfCacheHead->page_id = -1;
  surfCacheHead->scale = 0;
  surfCacheHead->search_results = NULL;
  PageSurfCache *last = surfCacheHead;
  for (int f = 1; f < SURF_CACHE_SIZE; ++f) {
    PageSurfCache *c = (PageSurfCache *)g_malloc0(sizeof(PageSurfCache));
    memset(c, 0, sizeof(PageSurfCache));
    c->page_id = -1;
    c->scale = 0;
    c->search_results = NULL;
    last->next = c;
    c->prev = last;
    last = c;
  }
  surfCacheTail = last;
}


//==========================================================================
//
//  surfCacheDestroy
//
//==========================================================================
static void surfCacheDestroy (void) {
  while (surfCacheHead) {
    PageSurfCache *c = surfCacheHead;
    surfCacheHead = c->next;
    if (c->surf) cairo_surface_destroy(c->surf);
    if (c->search_results) g_list_free(c->search_results);
    g_free(c);
  }
  surfCacheHead = surfCacheTail = NULL;
}


//==========================================================================
//
//  surfCacheRemove
//
//==========================================================================
static void surfCacheRemove (PageSurfCache *c) {
  if (!c) return;
  if (c->prev) c->prev->next = c->next; else surfCacheHead = c->next;
  if (c->next) c->next->prev = c->prev; else surfCacheTail = c->prev;
  c->prev = c->next = NULL;
}


//==========================================================================
//
//  surfCacheToFront
//
//==========================================================================
static void surfCacheToFront (PageSurfCache *c) {
  if (!c || c == surfCacheHead) return; // nothing to do
  surfCacheRemove(c);
  // make first
  c->next = surfCacheHead;
  surfCacheHead->prev = c;
  surfCacheHead = c;
}


//==========================================================================
//
//  surfCacheToBack
//
//==========================================================================
static void surfCacheToBack (PageSurfCache *c) {
  if (!c || c == surfCacheTail) return; // nothing to do
  surfCacheRemove(c);
  // make last
  surfCacheTail->next = c;
  c->prev = surfCacheTail;
  surfCacheHead = c;
}


//==========================================================================
//
//  surfCacheGet
//
//  this either returns an existing cached item, or creates a new
//  one, of evicts and clears the old one
//  the item is also moved to the front of the list
//  destroys the surface if it is invalid (i.e. check the `surf`
//  field in the returned struct)
//
//==========================================================================
static PageSurfCache *surfCacheGet (int pgid, const double scale) {
  surfCacheInit();
  for (PageSurfCache *c = surfCacheHead; c; c = c->next) {
    if (c->page_id == pgid) {
      surfCacheToFront(c);
      if (c->scale != scale) {
        if (c->surf) cairo_surface_destroy(c->surf);
        if (c->search_results) g_list_free(c->search_results);
        c->surf = NULL;
        c->scale = scale;
        c->search_results = NULL;
      }
      return c;
    }
  }
  // not found, use tail
  PageSurfCache *res = surfCacheTail;
  surfCacheToFront(res);
  if (res->surf) cairo_surface_destroy(res->surf);
  if (res->search_results) g_list_free(res->search_results);
  res->surf = NULL;
  res->page_id = pgid;
  res->scale = scale;
  res->search_results = NULL;
  return res;
}


//==========================================================================
//
//  surfCacheFind
//
//  doesn't care about scale
//  returns only pages with created cairo surfaces
//  doesn't move page around
//
//==========================================================================
static PageSurfCache *surfCacheFind (int pgid) {
  if (!surfCacheHead) return NULL;
  for (PageSurfCache *c = surfCacheHead; c; c = c->next) {
    if (c->page_id == pgid && c->surf) return c;
  }
  return NULL;
}


//==========================================================================
//
//  surfCacheEvictPage
//
//==========================================================================
static void surfCacheEvictPage (int pgid) {
  for (PageSurfCache *c = surfCacheHead; c; c = c->next) {
    if (c->page_id == pgid) {
      surfCacheToBack(c);
      if (c->surf) cairo_surface_destroy(c->surf);
      if (c->search_results) g_list_free(c->search_results);
      c->page_id = -1;
      c->surf = NULL;
      c->scale = 0;
      c->search_results = NULL;
      return;
    }
  }
}


//==========================================================================
//
//  surfCacheEvictHighlighted
//
//  evict all pages with highlighted search results
//
//==========================================================================
void surfCacheEvictHighlighted (void) {
  PageSurfCache *el = surfCacheTail;
  while (el) {
    PageSurfCache *c = el;
    el = el->prev;
    if (c->search_results) {
      if (c->surf) cairo_surface_destroy(c->surf);
      if (c->search_results) g_list_free(c->search_results);
      c->page_id = -1;
      c->surf = NULL;
      c->scale = 0;
      c->search_results = NULL;
      surfCacheToBack(c);
    }
  }
}


/* enums */
enum {
  NEXT, PREVIOUS, LEFT, RIGHT, UP, DOWN, BOTTOM, TOP, HIDE, HIGHLIGHT,
  DELETE_LAST_WORD, DELETE_LAST_CHAR, DEFAULT, ERROR, WARNING, NEXT_GROUP,
  PREVIOUS_GROUP, ZOOM_IN, ZOOM_OUT, ZOOM_ORIGINAL, ZOOM_SPECIFIC, FORWARD,
  BACKWARD, ADJUST_BESTFIT, ADJUST_WIDTH, ADJUST_NONE, CONTINUOUS, DELETE_LAST,
  ADD_MARKER, EVAL_MARKER, EXPAND, COLLAPSE, SELECT, GOTO_DEFAULT, GOTO_LABELS,
  GOTO_OFFSET, FULL_UP, FULL_DOWN, FULL_WINDOW_DOWN,
  NEXT_CHAR, PREVIOUS_CHAR, DELETE_TO_LINE_START, APPEND_FILEPATH,
  NO_SEARCH_UP, NO_SEARCH_DOWN,

  WINDOW_UP, WINDOW_DOWN,

  LAST,
  OPEN_COMPLETION_LAST,

  CENTER_LEFT_TOP, CENTER_CENTER, CENTER_RIGHT_BOTTOM,
  XOFFSET_DECREMENT, XOFFSET_INCREMENT,
  XOFFSET_DECREMENT_SLOW, XOFFSET_INCREMENT_SLOW,
};


/* define modes */
#define ALL         (1<<0)
#define FULLSCREEN  (1<<1)
#define INDEX       (1<<2)
#define NORMAL      (1<<3)


/* typedefs */
typedef struct CElement {
  char *value;
  char *description;
  struct CElement *next;
} CompletionElement;


typedef struct CGroup {
  char *value;
  CompletionElement *elements;
  struct CGroup *next;
} CompletionGroup;



typedef struct {
  CompletionGroup *groups;
} Completion;


typedef struct {
  char *command;
  char *description;
  int command_id;
  gboolean is_group;
  GtkWidget *row;
} CompletionRow;


typedef struct {
  int n;
  void *data;
} Argument;


typedef struct {
  char *name;
  int argument;
} ArgumentName;


typedef struct {
  int mask;
  int key;
  void (*function) (Argument *);
  int mode;
  Argument argument;
} Shortcut;


typedef struct {
  char *name;
  int mode;
  char *display;
} ModeName;


typedef struct {
  char *name;
  void (*function) (Argument *);
} ShortcutName;


typedef struct {
  int mask;
  int key;
  void (*function) (Argument *);
  Argument argument;
} InputbarShortcut;


typedef struct {
  int direction;
  void (*function) (Argument *);
  Argument argument;
} MouseScrollEvent;


typedef struct {
  char *command;
  char *abbr;
  gboolean (*function) (int, char **);
  Completion* (*completion) (char *);
  char *description;
} Command;


typedef struct {
  char *regex;
  void (*function) (char *, Argument *);
  Argument argument;
} BufferCommand;


typedef struct {
  char identifier;
  gboolean (*function) (char *, Argument *);
  int always;
  Argument argument;
} SpecialCommand;


typedef struct SCList {
  Shortcut element;
  struct SCList *next;
} ShortcutList;


typedef struct {
  char *identifier;
  int key;
} GDKKey;


typedef struct {
  PopplerPage *page;
  int id;
  char *label;
  double width;
  double height;
} Page;


typedef struct {
  char *name;
  void *variable;
  char type;
  gboolean render;
  gboolean reinit;
  char *description;
} Setting;


typedef struct {
  int id;
  int page;
} Marker;


typedef struct {
  char *id;
  int page;
} Bookmark;


/* zathura */
static struct {
  struct {
    GtkWidget *window;
    GtkBox *box;
    GtkBox *continuous;
    GtkScrolledWindow *view;
    GtkViewport *viewport;
    GtkWidget *statusbar;
    GtkBox *statusbar_entries;
    GtkEntry *inputbar;
    GtkWidget *index;
    GtkWidget *information;
    GtkWidget *drawing_area;
    GtkWidget *document;
    GdkNativeWindow embed;
    gint scroll_timer_id; /* 0: none */
    int scroll_step;
    int scroll_destination;
  } UI;

  struct {
    GdkColor default_fg;
    GdkColor default_bg;
    GdkColor inputbar_fg;
    GdkColor inputbar_bg;
    GdkColor statusbar_fg;
    GdkColor statusbar_bg;
    GdkColor completion_fg;
    GdkColor completion_bg;
    GdkColor completion_g_bg;
    GdkColor completion_g_fg;
    GdkColor completion_hl_fg;
    GdkColor completion_hl_bg;
    GdkColor notification_e_fg;
    GdkColor notification_e_bg;
    GdkColor notification_w_fg;
    GdkColor notification_w_bg;
    GdkColor recolor_darkcolor;
    GdkColor recolor_lightcolor;
    GdkColor search_highlight;
    GdkColor select_text;
    PangoFontDescription *font;
  } Style;

  struct {
    GtkLabel *status_text;
    GtkLabel *status_buffer;
    GtkLabel *status_state;
    GString *buffer;
    GList *history;
    int mode;
    int viewing_mode;
    gboolean recolor;
    gboolean enable_labelmode;
    int goto_mode;
    int adjust_mode;
    gboolean show_index;
    gboolean show_statusbar;
    gboolean show_inputbar;
    int xcenter_mode;
  } Global;

  struct {
    ShortcutList *sclist;
  } Bindings;

  struct {
    gdouble x;
    gdouble y;
  } SelectPoint;

  struct {
    char *filename;
    char *pages;
    int scroll_percentage;
  } State;

  struct {
    Marker *markers;
    int number_of_markers;
    int last;
  } Marker;

  struct {
    GFileMonitor *monitor;
    GFile *file;
  } FileMonitor;

  struct {
    GKeyFile *data;
    char *file;
    Bookmark *bookmarks;
    int number_of_bookmarks;
  } Bookmarks;

  struct {
    PopplerDocument *document;
    char *file;
    char *password;
    Page **pages;
    int page_number;
    double page_yskip_t; // [0..1]
    int page_yskip_pix; // in screen pixels; for a gap
    int number_of_pages;
    int scale;
    int rotate;
    int page_gap; // usually 8
    //TODO: cache total height and other things?
    int invert_page_scroll_mode;
    int xcenter_offset;
    int xleft_offset;
    int xright_offset;
  } PDF;

  struct {
    gboolean active;
    gchar *query;
  } Search;

  struct {
    guint inputbar_activate;
    guint inputbar_key_press_event;
  } Handler;

  struct {
    gchar *config_dir;
    gchar *data_dir;
  } Config;

  struct {
    gchar *file;
  } StdinSupport;
} Zathura;


/* function declarations */
void init_look (void);
void init_directories (void);
void init_bookmarks (void);
void init_keylist (void);
void init_settings (void);
void init_zathura (void);
void add_marker (int);
void build_index (GtkTreeModel *, GtkTreeIter *, PopplerIndexIter *);
void change_mode (int);
void calculate_screen_offset (GtkWidget *widget, int scrx, int scry, double *offset_x, double *offset_y, int *out_page_id);
void close_file (gboolean);
void enter_password (void);
void highlight_result (int page_id, PopplerRectangle *rectangle);
cairo_surface_t *create_page_surface (int page_id);
void draw (int page_id);
void render_view (GtkWidget *widget, gboolean fullclear);
void eval_marker (int);
void notify (int, const char *);
gboolean open_file (char *, char *);
gboolean open_stdin (gchar *);
void open_uri (char *);
void out_of_memory (void) NORETURN;
void update_status (void);
void read_bookmarks_file (void);
void write_bookmarks_file (void);
void free_bookmarks (void);
void read_configuration_file (const char *);
void read_configuration (void);
void recalc_rectangle (int, PopplerRectangle *);
void set_completion_row_color (GtkBox *, int, int);
void set_page (int);
void set_page_keep_ofs (int page);
void switch_view (GtkWidget *);
GtkEventBox *create_completion_row (GtkBox *, char *, char *, gboolean);
gchar *fix_path (const gchar *);
gchar *path_from_env (const gchar *);
gchar *get_home_dir (void);

Completion *completion_init (void);
CompletionGroup *completion_group_create (char*);
void completion_add_group (Completion *, CompletionGroup *);
void completion_free (Completion *);
void completion_group_add_element (CompletionGroup *, char *, char *);
void completion_group_prepend_element (CompletionGroup *, char *, char *);
void completion_group_limit_last (CompletionGroup *group, int count);

void abort_smooth_scrolling (void);
void setup_smooth_scroll_to (int destination, int delta);

/* shortcut declarations */
void sc_abort (Argument *);
void sc_adjust_window (Argument *);
void sc_xcenter_window (Argument *);
void sc_xoffset_modify (Argument *);
//void sc_ycenter_window (Argument *);
void sc_change_buffer (Argument *);
void sc_change_mode (Argument *);
void sc_focus_inputbar (Argument *);
void sc_follow (Argument *);
void sc_navigate (Argument *);
void sc_recolor (Argument *);
void sc_reload (Argument *);
void sc_rotate (Argument *);
void sc_scroll (Argument *);
void sc_search (Argument *);
void sc_switch_goto_mode (Argument *);
void sc_navigate_index (Argument *);
void sc_toggle_index (Argument *);
void sc_toggle_inputbar (Argument *);
void sc_toggle_fullscreen (Argument *);
void sc_toggle_statusbar (Argument *);
void sc_quit (Argument *);
void sc_zoom (Argument *);

/* inputbar shortcut declarations */
void isc_abort (Argument *);
void isc_command_history (Argument *);
void isc_completion (Argument *);
void isc_string_manipulation (Argument *);

/* command declarations */
gboolean cmd_bookmark (int, char **);
gboolean cmd_open_bookmark (int, char **);
gboolean cmd_close (int, char **);
gboolean cmd_delete_bookmark (int, char **);
gboolean cmd_export (int, char **);
gboolean cmd_info (int, char **);
gboolean cmd_map (int, char **);
gboolean cmd_open (int, char **);
gboolean cmd_history (int, char **);
gboolean cmd_print (int, char **);
gboolean cmd_rotate (int, char **);
gboolean cmd_set (int, char **);
gboolean cmd_quit (int, char **);
gboolean cmd_save (int, char **);
gboolean cmd_savef (int, char **);

/* completion commands */
Completion *cc_bookmark (char *);
Completion *cc_export (char *);
Completion *cc_open (char *);
Completion *cc_history (char *);
Completion *cc_print (char *);
Completion *cc_set (char *);

/* buffer command declarations */
void bcmd_evalmarker (char *, Argument *);
void bcmd_goto (char *, Argument *);
void bcmd_scroll (char *, Argument *);
void bcmd_setmarker (char *, Argument *);
void bcmd_zoom (char *, Argument *);
void bcmd_pgsc_toggle (char *buffer, Argument *argument);

/* special command delcarations */
gboolean scmd_search (gchar *, Argument *);

/* callback declarations */
gboolean cb_destroy (GtkWidget *, gpointer);
gboolean cb_draw (GtkWidget *, GdkEventExpose *, gpointer);
gboolean cb_index_row_activated (GtkTreeView *, GtkTreePath *, GtkTreeViewColumn *, gpointer);
gboolean cb_inputbar_kb_pressed (GtkWidget *, GdkEventKey *, gpointer);
gboolean cb_inputbar_activate (GtkEntry *, gpointer);
gboolean cb_inputbar_form_activate (GtkEntry *, gpointer);
gboolean cb_inputbar_password_activate (GtkEntry *, gpointer);
gboolean cb_view_kb_pressed (GtkWidget *, GdkEventKey *, gpointer);
gboolean cb_view_resized (GtkWidget *, GtkAllocation *, gpointer);
gboolean cb_view_button_pressed (GtkWidget *, GdkEventButton *, gpointer);
gboolean cb_view_button_release (GtkWidget *, GdkEventButton *, gpointer);
gboolean cb_view_motion_notify (GtkWidget *, GdkEventMotion *, gpointer);
gboolean cb_view_scrolled (GtkWidget *, GdkEventScroll *, gpointer);
gboolean cb_watch_file (GFileMonitor *, GFile *, GFile *, GFileMonitorEvent, gpointer);


/* configuration */
#include "config.h"


//==========================================================================
//
//  safe_realloc
//
//==========================================================================
static void *safe_realloc (void **ptr, size_t nmemb, size_t size) {
  static const size_t limit = ~((size_t)0u);
  void *tmp = NULL;
  /* Check for overflow. */
  if (nmemb > limit/size) goto failure;
  tmp = realloc(*ptr, nmemb*size);
  /* Check for out of memory. */
  if (!tmp) goto failure;
  *ptr = tmp;
  return *ptr;
/* Error handling. */
failure:
  free(*ptr);
  *ptr = NULL;
  return NULL;
}


//==========================================================================
//
//  sys_time
//
//==========================================================================
static double sys_time (void) {
  struct timeval tp;
  struct timezone tzp;
  gettimeofday(&tp, &tzp);
  return (double)(tp.tv_sec)+(double)tp.tv_usec/1000000.0;
}


//==========================================================================
//
//  get_page_scroll_mode
//
//==========================================================================
static int get_page_scroll_mode (void) {
  return (Zathura.PDF.invert_page_scroll_mode ? !page_scroll_mode : !!page_scroll_mode);
}


//==========================================================================
//
//  init_look
//
//==========================================================================
void init_look (void) {
  /* parse  */
  gdk_color_parse(default_fgcolor,        &(Zathura.Style.default_fg));
  gdk_color_parse(default_bgcolor,        &(Zathura.Style.default_bg));
  gdk_color_parse(inputbar_fgcolor,       &(Zathura.Style.inputbar_fg));
  gdk_color_parse(inputbar_bgcolor,       &(Zathura.Style.inputbar_bg));
  gdk_color_parse(statusbar_fgcolor,      &(Zathura.Style.statusbar_fg));
  gdk_color_parse(statusbar_bgcolor,      &(Zathura.Style.statusbar_bg));
  gdk_color_parse(completion_fgcolor,     &(Zathura.Style.completion_fg));
  gdk_color_parse(completion_bgcolor,     &(Zathura.Style.completion_bg));
  gdk_color_parse(completion_g_fgcolor,   &(Zathura.Style.completion_g_fg));
  gdk_color_parse(completion_g_fgcolor,   &(Zathura.Style.completion_g_fg));
  gdk_color_parse(completion_hl_fgcolor,  &(Zathura.Style.completion_hl_fg));
  gdk_color_parse(completion_hl_bgcolor,  &(Zathura.Style.completion_hl_bg));
  gdk_color_parse(notification_e_fgcolor, &(Zathura.Style.notification_e_fg));
  gdk_color_parse(notification_e_bgcolor, &(Zathura.Style.notification_e_bg));
  gdk_color_parse(notification_w_fgcolor, &(Zathura.Style.notification_w_fg));
  gdk_color_parse(notification_w_bgcolor, &(Zathura.Style.notification_w_bg));
  gdk_color_parse(recolor_darkcolor,      &(Zathura.Style.recolor_darkcolor));
  gdk_color_parse(recolor_lightcolor,     &(Zathura.Style.recolor_lightcolor));
  gdk_color_parse(search_highlight,       &(Zathura.Style.search_highlight));
  gdk_color_parse(select_text,            &(Zathura.Style.select_text));

  pango_font_description_free(Zathura.Style.font);
  Zathura.Style.font = pango_font_description_from_string(font);

  /* window and viewport */
  gtk_widget_modify_bg(GTK_WIDGET(Zathura.UI.window),   GTK_STATE_NORMAL, &(Zathura.Style.default_bg));
  gtk_widget_modify_bg(GTK_WIDGET(Zathura.UI.viewport), GTK_STATE_NORMAL, &(Zathura.Style.default_bg));

  /* drawing area */
  gtk_widget_modify_bg(GTK_WIDGET(Zathura.UI.drawing_area), GTK_STATE_NORMAL, &(Zathura.Style.default_bg));

  /* statusbar */
  gtk_widget_modify_bg(GTK_WIDGET(Zathura.UI.statusbar), GTK_STATE_NORMAL, &(Zathura.Style.statusbar_bg));

  gtk_widget_modify_fg(GTK_WIDGET(Zathura.Global.status_text),  GTK_STATE_NORMAL, &(Zathura.Style.statusbar_fg));
  gtk_widget_modify_fg(GTK_WIDGET(Zathura.Global.status_state), GTK_STATE_NORMAL, &(Zathura.Style.statusbar_fg));
  gtk_widget_modify_fg(GTK_WIDGET(Zathura.Global.status_buffer), GTK_STATE_NORMAL, &(Zathura.Style.statusbar_fg));

  gtk_widget_modify_font(GTK_WIDGET(Zathura.Global.status_text),  Zathura.Style.font);
  gtk_widget_modify_font(GTK_WIDGET(Zathura.Global.status_state), Zathura.Style.font);
  gtk_widget_modify_font(GTK_WIDGET(Zathura.Global.status_buffer), Zathura.Style.font);

  /* inputbar */
  gtk_widget_modify_base(GTK_WIDGET(Zathura.UI.inputbar), GTK_STATE_NORMAL, &(Zathura.Style.inputbar_bg));
  gtk_widget_modify_text(GTK_WIDGET(Zathura.UI.inputbar), GTK_STATE_NORMAL, &(Zathura.Style.inputbar_fg));
  gtk_widget_modify_font(GTK_WIDGET(Zathura.UI.inputbar),                     Zathura.Style.font);

  g_object_set(G_OBJECT(Zathura.UI.inputbar), "has-frame", FALSE, NULL);

  /* scrollbars */
  if (show_scrollbars) {
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(Zathura.UI.view), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  } else {
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(Zathura.UI.view), GTK_POLICY_NEVER, GTK_POLICY_NEVER);
  }

  /* inputbar */
  if (Zathura.Global.show_inputbar) {
    gtk_widget_show(GTK_WIDGET(Zathura.UI.inputbar));
  } else {
    gtk_widget_hide(GTK_WIDGET(Zathura.UI.inputbar));
  }

  /* statusbar */
  if (Zathura.Global.show_statusbar) {
    gtk_widget_show(GTK_WIDGET(Zathura.UI.statusbar));
  } else {
    gtk_widget_hide(GTK_WIDGET(Zathura.UI.statusbar));
  }
}


//==========================================================================
//
//  fix_path
//
//==========================================================================
gchar *fix_path (const gchar *path) {
  if (!path) return NULL;
  if (path[0] == '~') {
    gchar *home_path = get_home_dir();
    gchar *res = g_build_filename(home_path, path+1, NULL);
    g_free(home_path);
    return res;
  }
  return g_strdup(path);
}


//==========================================================================
//
//  path_from_env
//
//==========================================================================
gchar *path_from_env (const gchar *var) {
  gchar *env = fix_path(g_getenv(var)), *res;
  if (!env) return NULL;
  res = g_build_filename(env, "zathura", NULL);
  g_free(env);
  return res;
}


//==========================================================================
//
//  get_home_dir
//
//==========================================================================
gchar *get_home_dir (void) {
  const gchar *homedir = g_getenv("HOME");
  return g_strdup(homedir ? homedir : g_get_home_dir());
}


//==========================================================================
//
//  init_directories
//
//==========================================================================
void init_directories (void) {
  /* setup directories */
  if (!Zathura.Config.config_dir) {
    #ifndef ZATHURA_NO_XDG
    gchar *env = path_from_env("XDG_CONFIG_HOME");
    if (env) Zathura.Config.config_dir = env;
    else
    #endif
      Zathura.Config.config_dir = fix_path(CONFIG_DIR);
  }
  if (!Zathura.Config.data_dir) {
    #ifndef ZATHURA_NO_XDG
    gchar *env = path_from_env("XDG_DATA_HOME");
    if (env) Zathura.Config.data_dir = env;
    else
    #endif
      Zathura.Config.data_dir = fix_path(DATA_DIR);
  }

  /* create zathura (config/data) directory */
  g_mkdir_with_parents(Zathura.Config.config_dir, 0771);
  g_mkdir_with_parents(Zathura.Config.data_dir,   0771);
}


//==========================================================================
//
//  init_bookmarks
//
//==========================================================================
void init_bookmarks (void) {
  /* init variables */
  Zathura.Bookmarks.number_of_bookmarks = 0;
  Zathura.Bookmarks.bookmarks = NULL;

  Zathura.Bookmarks.file = g_build_filename(Zathura.Config.data_dir, BOOKMARK_FILE, NULL);
  //fprintf(stderr, "CFG: %s\n", Zathura.Bookmarks.file);
  read_bookmarks_file();
}


//==========================================================================
//
//  is_reserved_bm_name
//
//==========================================================================
static gboolean is_reserved_bm_name (const char *bm_name) {
  for (int i = 0; i < BM_MAX; ++i) if (strcmp(bm_reserved_names[i], bm_name) == 0) return TRUE;
  return FALSE;
}


//==========================================================================
//
//  init_keylist
//
//==========================================================================
void init_keylist (void) {
  ShortcutList *e = NULL, *p = NULL;
  for (int i = 0; i < LENGTH(shortcuts); ++i) {
    e = malloc(sizeof(ShortcutList));
    if (!e) out_of_memory();
    e->element = shortcuts[i];
    e->next = NULL;
    if (!Zathura.Bindings.sclist) Zathura.Bindings.sclist = e;
    if (p) p->next = e;
    p = e;
  }
}


//==========================================================================
//
//  init_settings
//
//==========================================================================
void init_settings (void) {
  Zathura.State.filename = g_strdup((char*) default_text);
  Zathura.Global.adjust_mode = adjust_open;
  Zathura.Global.xcenter_mode = xcenter_open;

  gtk_window_set_default_size(GTK_WINDOW(Zathura.UI.window), default_width, default_height);
  if (default_maximize) gtk_window_maximize(GTK_WINDOW(Zathura.UI.window));
}


//==========================================================================
//
//  init_zathura
//
//==========================================================================
void init_zathura (void) {
  /* other */
  Zathura.Global.mode = NORMAL;
  Zathura.Global.viewing_mode = NORMAL;
  Zathura.Global.recolor = 0;
  Zathura.Global.goto_mode = GOTO_MODE;
  Zathura.Global.show_index = FALSE;
  Zathura.Global.show_inputbar = TRUE;
  Zathura.Global.show_statusbar = TRUE;

  Zathura.State.pages = g_strdup("");
  Zathura.State.scroll_percentage = 0;

  Zathura.Marker.markers = NULL;
  Zathura.Marker.number_of_markers = 0;
  Zathura.Marker.last = -1;

  Zathura.Search.active = FALSE;
  Zathura.Search.query = NULL;

  Zathura.FileMonitor.monitor = NULL;
  Zathura.FileMonitor.file = NULL;

  Zathura.StdinSupport.file = NULL;

  /* window */
  if (Zathura.UI.embed) {
    Zathura.UI.window = gtk_plug_new(Zathura.UI.embed);
  } else {
    Zathura.UI.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  }

  /* UI */
  Zathura.UI.box = GTK_BOX(gtk_vbox_new(FALSE, 0));
  Zathura.UI.continuous = GTK_BOX(gtk_vbox_new(FALSE, 0));
  Zathura.UI.view = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new(NULL, NULL));
  Zathura.UI.viewport = GTK_VIEWPORT(gtk_viewport_new(NULL, NULL));
  Zathura.UI.drawing_area = gtk_drawing_area_new();
  Zathura.UI.statusbar = gtk_event_box_new();
  Zathura.UI.statusbar_entries = GTK_BOX(gtk_hbox_new(FALSE, 0));
  Zathura.UI.inputbar = GTK_ENTRY(gtk_entry_new());
  Zathura.UI.document = gtk_event_box_new();

  /* window */
  gtk_window_set_title(GTK_WINDOW(Zathura.UI.window), "zathura");
  GdkGeometry hints = { 1, 1 };
  gtk_window_set_geometry_hints(GTK_WINDOW(Zathura.UI.window), NULL, &hints, GDK_HINT_MIN_SIZE);
  g_signal_connect(G_OBJECT(Zathura.UI.window), "destroy", G_CALLBACK(cb_destroy), NULL);

  /* box */
  gtk_box_set_spacing(Zathura.UI.box, 0);
  gtk_container_add(GTK_CONTAINER(Zathura.UI.window), GTK_WIDGET(Zathura.UI.box));

  /* continuous */
  gtk_box_set_spacing(Zathura.UI.continuous, 5);

  /* events */
  gtk_container_add(GTK_CONTAINER(Zathura.UI.document), GTK_WIDGET(Zathura.UI.drawing_area));
  gtk_widget_add_events(GTK_WIDGET(Zathura.UI.document), GDK_POINTER_MOTION_MASK|GDK_POINTER_MOTION_HINT_MASK|GDK_BUTTON_PRESS_MASK|GDK_BUTTON_RELEASE_MASK);

  g_signal_connect(G_OBJECT(Zathura.UI.document), "button-press-event", G_CALLBACK(cb_view_button_pressed), NULL);
  g_signal_connect(G_OBJECT(Zathura.UI.document), "button-release-event", G_CALLBACK(cb_view_button_release), NULL);
  g_signal_connect(G_OBJECT(Zathura.UI.document), "motion-notify-event", G_CALLBACK(cb_view_motion_notify),  NULL);
  gtk_widget_show(Zathura.UI.document);

  /* view */
  g_signal_connect(G_OBJECT(Zathura.UI.view), "key-press-event", G_CALLBACK(cb_view_kb_pressed), NULL);
  g_signal_connect(G_OBJECT(Zathura.UI.view), "size-allocate", G_CALLBACK(cb_view_resized), NULL);
  g_signal_connect(G_OBJECT(Zathura.UI.view), "scroll-event", G_CALLBACK(cb_view_scrolled), NULL);
  gtk_container_add(GTK_CONTAINER(Zathura.UI.view), GTK_WIDGET(Zathura.UI.viewport));
  gtk_viewport_set_shadow_type(Zathura.UI.viewport, GTK_SHADOW_NONE);

  /* drawing area */
  gtk_widget_show(Zathura.UI.drawing_area);
  g_signal_connect(G_OBJECT(Zathura.UI.drawing_area), "expose-event", G_CALLBACK(cb_draw), NULL);

  /* statusbar */
  Zathura.Global.status_text = GTK_LABEL(gtk_label_new(NULL));
  Zathura.Global.status_state = GTK_LABEL(gtk_label_new(NULL));
  Zathura.Global.status_buffer = GTK_LABEL(gtk_label_new(NULL));

  gtk_misc_set_alignment(GTK_MISC(Zathura.Global.status_text),  0.0, 0.0);
  gtk_misc_set_alignment(GTK_MISC(Zathura.Global.status_state), 1.0, 0.0);
  gtk_misc_set_alignment(GTK_MISC(Zathura.Global.status_buffer), 1.0, 0.0);

  gtk_misc_set_padding(GTK_MISC(Zathura.Global.status_text),  2.0, 4.0);
  gtk_misc_set_padding(GTK_MISC(Zathura.Global.status_state), 2.0, 4.0);
  gtk_misc_set_padding(GTK_MISC(Zathura.Global.status_buffer), 2.0, 4.0);

  gtk_label_set_use_markup(Zathura.Global.status_text,  TRUE);
  gtk_label_set_use_markup(Zathura.Global.status_state, TRUE);
  gtk_label_set_use_markup(Zathura.Global.status_buffer, TRUE);

  gtk_box_pack_start(Zathura.UI.statusbar_entries, GTK_WIDGET(Zathura.Global.status_text),  TRUE,  TRUE,  2);
  gtk_box_pack_start(Zathura.UI.statusbar_entries, GTK_WIDGET(Zathura.Global.status_buffer), FALSE, FALSE, 2);
  gtk_box_pack_start(Zathura.UI.statusbar_entries, GTK_WIDGET(Zathura.Global.status_state), FALSE, FALSE, 2);

  gtk_container_add(GTK_CONTAINER(Zathura.UI.statusbar), GTK_WIDGET(Zathura.UI.statusbar_entries));

  /* inputbar */
  gtk_entry_set_inner_border(Zathura.UI.inputbar, NULL);
  gtk_entry_set_has_frame(   Zathura.UI.inputbar, FALSE);
  gtk_editable_set_editable( GTK_EDITABLE(Zathura.UI.inputbar), TRUE);

  Zathura.Handler.inputbar_key_press_event = g_signal_connect(G_OBJECT(Zathura.UI.inputbar), "key-press-event", G_CALLBACK(cb_inputbar_kb_pressed), NULL);
  Zathura.Handler.inputbar_activate = g_signal_connect(G_OBJECT(Zathura.UI.inputbar), "activate", G_CALLBACK(cb_inputbar_activate), NULL);

  /* packing */
  gtk_box_pack_start(Zathura.UI.box, GTK_WIDGET(Zathura.UI.view), TRUE, TRUE, 0);
  gtk_box_pack_start(Zathura.UI.box, GTK_WIDGET(Zathura.UI.statusbar), FALSE, FALSE, 0);
  gtk_box_pack_end(Zathura.UI.box, GTK_WIDGET(Zathura.UI.inputbar), FALSE, FALSE, 0);
}


//==========================================================================
//
//  add_marker
//
//==========================================================================
void add_marker (int id) {
  if (id < 0x30 || id > 0x7A) return;
  /* current information */
  int page_number = Zathura.PDF.page_number;
  /* search if entry already exists */
  for (int i = 0; i < Zathura.Marker.number_of_markers; ++i) {
    if (Zathura.Marker.markers[i].id == id) {
      Zathura.Marker.markers[i].page = page_number;
      Zathura.Marker.last = page_number;
      return;
    }
  }
  /* add new marker */
  int marker_index = Zathura.Marker.number_of_markers++;
  Zathura.Marker.markers = safe_realloc((void**)&Zathura.Marker.markers, Zathura.Marker.number_of_markers, sizeof(Marker));
  if (!Zathura.Marker.markers) out_of_memory();
  Zathura.Marker.markers[marker_index].id = id;
  Zathura.Marker.markers[marker_index].page = page_number;
  Zathura.Marker.last = page_number;
}


//==========================================================================
//
//  build_index
//
//==========================================================================
void build_index (GtkTreeModel *model, GtkTreeIter *parent, PopplerIndexIter *index_iter) {
  do {
    GtkTreeIter tree_iter;
    PopplerIndexIter *child;
    PopplerAction *action;
    gchar *markup;

    action = poppler_index_iter_get_action(index_iter);
    if (!action) continue;

    markup = g_markup_escape_text (action->any.title, -1);

    gtk_tree_store_append(GTK_TREE_STORE(model), &tree_iter, parent);
    gtk_tree_store_set(GTK_TREE_STORE(model), &tree_iter, 0, markup, 1, action, -1);
    g_object_weak_ref(G_OBJECT(model), (GWeakNotify) poppler_action_free, action);
    g_free(markup);

    child = poppler_index_iter_get_child(index_iter);
    if (child) build_index(model, &tree_iter, child);
    poppler_index_iter_free(child);
  } while (poppler_index_iter_next(index_iter));
}


//==========================================================================
//
//  get_page_screen_height
//
//==========================================================================
int get_page_screen_height (int page_id) {
  if (!Zathura.PDF.document || Zathura.PDF.number_of_pages < 1) return 0;
  if (page_id < 0 || page_id >= Zathura.PDF.number_of_pages) return 0;
  const double scale = ((double)Zathura.PDF.scale/100.0);
  Page *current_page = Zathura.PDF.pages[page_id];
  return (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width)*scale+Zathura.PDF.page_yskip_pix;
}


//==========================================================================
//
//  get_current_page_screen_ytop
//
//==========================================================================
int get_current_page_screen_ytop (void) {
  if (!Zathura.PDF.document || Zathura.PDF.number_of_pages < 1) return 0;
  const int page_id = Zathura.PDF.page_number;
  if (page_id < 0) return 0;
  const double scale = ((double)Zathura.PDF.scale/100.0);
  if (page_id >= Zathura.PDF.number_of_pages) {
    // beyond the last page, show the whole last page
    if (!Zathura.UI.view) return 0;
    GtkAdjustment *adjustment = gtk_scrolled_window_get_vadjustment(Zathura.UI.view);
    int window_y = gtk_adjustment_get_page_size(adjustment);
    Page *current_page = Zathura.PDF.pages[Zathura.PDF.number_of_pages-1];
    const int scheight = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width)*scale+Zathura.PDF.page_gap;
    return (scheight >= window_y ? scheight-window_y : 0);
  } else {
    Page *current_page = Zathura.PDF.pages[Zathura.PDF.page_number];
    const double height = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width);
    return -(height*Zathura.PDF.page_yskip_t*scale+Zathura.PDF.page_yskip_pix);
  }
}


//==========================================================================
//
//  calculate_screen_offset
//
//  calculates screen page offset (and id) in the widget
//
//==========================================================================
void calculate_screen_offset (GtkWidget *widget, int scrx, int scry, double *offset_x, double *offset_y, int *out_page_id) {
  const double scale = ((double)Zathura.PDF.scale/100.0);
  int ypos = get_current_page_screen_ytop();
  int page_id = Zathura.PDF.page_number;

  int window_x, window_y;
  gdk_drawable_get_size(widget->window, &window_x, &window_y);

  //fprintf(stderr, "=== scr:(%d,%d); ypos=%d; page_id=%d; win:(%d,%d) ===\n", scrx, scry, ypos, page_id, window_x, window_y);
  while (ypos < window_y && page_id < Zathura.PDF.number_of_pages) {
    Page *current_page = Zathura.PDF.pages[page_id];
    const int scheight = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width)*scale;
    if (scry >= ypos && scry < ypos+scheight) {
      // this page!
      const int scwidth = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->width : current_page->height)*scale;
      if (offset_x) {
        if (window_x > scwidth) {
          switch (Zathura.Global.xcenter_mode) {
            case CENTER_LEFT_TOP: *offset_x = Zathura.PDF.xleft_offset; break;
            case CENTER_RIGHT_BOTTOM: *offset_x = window_x-scwidth+Zathura.PDF.xright_offset; break;
            default: *offset_x = (window_x-scwidth)/2+Zathura.PDF.xcenter_offset; break;
          }
        } else {
          *offset_x = 0;
        }
      }
      if (offset_y) *offset_y = ypos;
      if (out_page_id) *out_page_id = page_id;
      //fprintf(stderr, "   RES: pos=(%g,%g); page_id=%d\n", *offset_x, *offset_y, *out_page_id);
      return;
    }
    ypos += scheight+Zathura.PDF.page_gap;
    page_id += 1;
  }

  if (out_page_id) *out_page_id = -1; // mark as invalid
  if (offset_x) *offset_x = 0;
  if (offset_y) *offset_y = 0;
}


//==========================================================================
//
//  calc_current_document_screen_offset
//
//==========================================================================
int calc_current_document_screen_offset (void) {
  if (!Zathura.PDF.document || Zathura.PDF.number_of_pages == 0) return 0;

  int dest_page_id = Zathura.PDF.page_number;
  if (dest_page_id < 0) dest_page_id = 1;
  if (dest_page_id >= Zathura.PDF.number_of_pages) dest_page_id = Zathura.PDF.number_of_pages-1;

  //fprintf(stderr, "curr_page_id=%d; t=%g; px=%d\n", dest_page_id, Zathura.PDF.page_yskip_t, Zathura.PDF.page_yskip_pix);

  const double scale = ((double)Zathura.PDF.scale/100.0);
  Page *current_page;

  int curryofs = 0;
  for (int page_id = 0; page_id < dest_page_id; ++page_id) {
    current_page = Zathura.PDF.pages[page_id];
    const int scheight = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width)*scale;
    curryofs += scheight+Zathura.PDF.page_gap;
  }

  current_page = Zathura.PDF.pages[dest_page_id];
  curryofs += ((Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width)*Zathura.PDF.page_yskip_t*scale+Zathura.PDF.page_yskip_pix);

  //fprintf(stderr, "curr_page_id=%d; t=%g; px=%d; curryofs=%d\n", dest_page_id, Zathura.PDF.page_yskip_t, Zathura.PDF.page_yskip_pix, curryofs);
  return curryofs;
}


//==========================================================================
//
//  calc_document_bottom_pos
//
//==========================================================================
void calc_document_bottom_pos (int *out_page_id, double *out_page_yskip_t, int *out_page_yskip_pix) {
  int tmppgid = 0, tmppgofs = 0;
  double tmpt = 0;
  if (!out_page_id) out_page_id = &tmppgid;
  if (!out_page_yskip_t) out_page_yskip_t = &tmpt;
  if (!out_page_yskip_pix) out_page_yskip_pix = &tmppgofs;

  if (!Zathura.PDF.document || Zathura.PDF.number_of_pages < 1) {
    *out_page_id = 0;
    *out_page_yskip_t = 0;
    *out_page_yskip_pix = 0;
    return;
  }

  const int page_id = Zathura.PDF.number_of_pages-1;

  *out_page_id = page_id;
  *out_page_yskip_t = 0;
  *out_page_yskip_pix = 0;

  if (!Zathura.UI.view) return;

  GtkAdjustment *adjustment = gtk_scrolled_window_get_vadjustment(Zathura.UI.view);
  int window_y = gtk_adjustment_get_page_size(adjustment);
  Page *current_page = Zathura.PDF.pages[page_id];
  const double scale = ((double)Zathura.PDF.scale/100.0);
  const int scheight = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width)*scale;
  if (scheight > window_y) {
    *out_page_yskip_pix = 0;
    int yskip = scheight-window_y;
    *out_page_yskip_t = (double)yskip/(double)scheight;
  }
}


//==========================================================================
//
//  convert_screen_document_offset_to_pos
//
//==========================================================================
void convert_screen_document_offset_to_pos (int docofs, int *out_page_id, double *out_page_yskip_t, int *out_page_yskip_pix) {
  int tmppgid = 0, tmppgofs = 0;
  double tmpt = 0;
  if (!out_page_id) out_page_id = &tmppgid;
  if (!out_page_yskip_t) out_page_yskip_t = &tmpt;
  if (!out_page_yskip_pix) out_page_yskip_pix = &tmppgofs;

  if (!Zathura.PDF.document || Zathura.PDF.number_of_pages < 1) {
    *out_page_id = 0;
    *out_page_yskip_t = 0;
    *out_page_yskip_pix = 0;
    return;
  }

  if (docofs < 0) docofs = 0;

  const double scale = ((double)Zathura.PDF.scale/100.0);

  //fprintf(stderr, "docofs=%d\n", docofs);

  int curryofs = 0;
  for (int page_id = 0; page_id < Zathura.PDF.number_of_pages; ++page_id) {
    Page *current_page = Zathura.PDF.pages[page_id];
    const double height = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width);
    const int scheight = height*scale;
    const int pgend = curryofs+scheight;
    if (docofs >= curryofs && docofs < pgend+Zathura.PDF.page_gap) {
      *out_page_id = page_id;
      if (docofs < pgend) {
        // some part of the page is visible
        *out_page_yskip_t = (double)(docofs-curryofs)/(double)scheight;
        *out_page_yskip_pix = 0;
      } else {
        // no page visible, we are inside a gap
        *out_page_yskip_t = 1;
        *out_page_yskip_pix = docofs-pgend;
      }
      // if this is the last page, don't allow to scroll it completely out of view
      if (Zathura.UI.view && page_id == Zathura.PDF.number_of_pages-1) {
        int pid, yofs;
        double t;
        calc_document_bottom_pos(&pid, &t, &yofs);
        if (*out_page_yskip_t > t) {
          *out_page_yskip_t = t;
          if (*out_page_yskip_pix > yofs) *out_page_yskip_pix = yofs;
        }
      }
      return;
    }
    curryofs = pgend+Zathura.PDF.page_gap;
  }

  // beyond the last page, show the whole last page
  calc_document_bottom_pos(out_page_id, out_page_yskip_t, out_page_yskip_pix);
}


//==========================================================================
//
//  scroll_up_pixels_no_draw
//
//==========================================================================
void scroll_up_pixels_no_draw (int count) {
  if (!count) return;
  int currypos = calc_current_document_screen_offset();
  currypos -= count;
  convert_screen_document_offset_to_pos(currypos, &Zathura.PDF.page_number, &Zathura.PDF.page_yskip_t, &Zathura.PDF.page_yskip_pix);
}


//==========================================================================
//
//  scroll_down_pixels_no_draw
//
//==========================================================================
void scroll_down_pixels_no_draw (int count) {
  if (!count) return;
  int currypos = calc_current_document_screen_offset();
  currypos += count;
  convert_screen_document_offset_to_pos(currypos, &Zathura.PDF.page_number, &Zathura.PDF.page_yskip_t, &Zathura.PDF.page_yskip_pix);
}


//==========================================================================
//
//  create_page_surface
//
//==========================================================================
cairo_surface_t *create_page_surface (int page_id) {
  if (!Zathura.PDF.document || page_id < 0 || page_id >= Zathura.PDF.number_of_pages) return NULL;

  const double scale = ((double)Zathura.PDF.scale/100.0);

  PageSurfCache *cc = surfCacheGet(page_id, scale);
  // create new surface if necessary
  if (!cc->surf) {
    if (cc->search_results) abort();

    //fprintf(stderr, "creating new page surface for page #%d\n", page_id);
    int rotate = Zathura.PDF.rotate;
    Page *current_page = Zathura.PDF.pages[page_id];
    cairo_surface_t *surf = NULL;
    const double page_width = current_page->width;
    const double page_height = current_page->height;

    double width, height;
    if (rotate == 0 || rotate == 180) {
      width = page_width*scale;
      height = page_height*scale;
    } else {
      width = page_height*scale;
      height = page_width*scale;
    }

    surf = cairo_image_surface_create(CAIRO_FORMAT_RGB24, width, height);

    cairo_t *cairo = cairo_create(surf);

    cairo_save(cairo);
    cairo_set_source_rgb(cairo, 1, 1, 1);
    cairo_rectangle(cairo, 0, 0, width, height);
    cairo_fill(cairo);
    cairo_restore(cairo);
    cairo_save(cairo);

    switch (rotate) {
      case 90: cairo_translate(cairo, width, 0); break;
      case 180: cairo_translate(cairo, width, height); break;
      case 270: cairo_translate(cairo, 0, height); break;
      default: cairo_translate(cairo, 0, 0); break;
    }

    if (scale != 1.0) cairo_scale(cairo, scale, scale);
    if (rotate != 0) cairo_rotate(cairo, rotate*G_PI/180.0);

    poppler_page_render(current_page->page, cairo);

    /* remove transations and such */
    cairo_restore(cairo);

    /* draw page results if there are any */
    if (Zathura.Search.active && Zathura.Search.query && Zathura.Search.query[0]) {
      cc->search_results = poppler_page_find_text(current_page->page, Zathura.Search.query);
      if (cc->search_results) {
        for (GList *list = cc->search_results; list && list->data; list = g_list_next(list)) {
          PopplerRectangle *trect = poppler_rectangle_copy((PopplerRectangle *)list->data);
          cairo_set_source_rgba(cairo, Zathura.Style.search_highlight.red, Zathura.Style.search_highlight.green, Zathura.Style.search_highlight.blue, transparency);
          recalc_rectangle(page_id, trect);
          cairo_rectangle(cairo, trect->x1, trect->y1, trect->x2-trect->x1, trect->y2-trect->y1);
          poppler_rectangle_free(trect);
          cairo_fill(cairo);
        }
      }
    }

    cairo_destroy(cairo);

    if (Zathura.Global.recolor) {
      unsigned char *image = cairo_image_surface_get_data(surf);
      int width = cairo_image_surface_get_width(surf);
      int height = cairo_image_surface_get_height(surf);
      int rowstride = cairo_image_surface_get_stride(surf);

      /* recolor code based on qimageblitz library flatten() function
      (http://sourceforge.net/projects/qimageblitz/) */

      int r1 = Zathura.Style.recolor_darkcolor.red/257;
      int g1 = Zathura.Style.recolor_darkcolor.green/257;
      int b1 = Zathura.Style.recolor_darkcolor.blue/257;
      int r2 = Zathura.Style.recolor_lightcolor.red/257;
      int g2 = Zathura.Style.recolor_lightcolor.green/257;
      int b2 = Zathura.Style.recolor_lightcolor.blue/257;

      int min = 0x00;
      int max = 0xFF;
      int mean;

      float sr = ((float)r2-r1)/(max-min);
      float sg = ((float)g2-g1)/(max-min);
      float sb = ((float)b2-b1)/(max-min);

      for (int y = 0; y < height; ++y) {
        unsigned char *data = image+y*rowstride;
        for (int x = 0; x < width; ++x) {
          mean = (data[0]+data[1]+data[2])/3;
          data[2] = sr*(mean-min)+r1+0.5;
          data[1] = sg*(mean-min)+g1+0.5;
          data[0] = sb*(mean-min)+b1+0.5;
          data += 4;
        }
      }
    }

    cc->surf = surf;
  } else {
    //fprintf(stderr, "reusing new page surface for page #%d\n", page_id);
  }

  return cc->surf;
}


//==========================================================================
//
//  draw
//
//==========================================================================
void draw (int page_id) {
  if (!Zathura.PDF.document || Zathura.PDF.number_of_pages < 1) return;
  if (page_id < 0) page_id = Zathura.PDF.page_number;

  cairo_surface_t *surf = create_page_surface(page_id);
  if (!surf) return;

  int w = cairo_image_surface_get_width(surf);
  int h = cairo_image_surface_get_height(surf);

  gtk_widget_set_size_request(Zathura.UI.drawing_area, w, h);
  gtk_widget_queue_draw(Zathura.UI.drawing_area);
}


//==========================================================================
//
//  change_mode
//
//==========================================================================
void change_mode (int mode) {
  char *mode_text = 0;
  for (unsigned int i = 0; i != LENGTH(mode_names); ++i) {
    if (mode_names[i].mode == mode) {
      mode_text = mode_names[i].display;
      break;
    }
  }
  if (!mode_text) {
    switch(mode) {
      case ADD_MARKER: mode_text = ""; break;
      case EVAL_MARKER: mode_text = ""; break;
      default: mode_text = ""; mode = NORMAL; break;
    }
  }
  Zathura.Global.mode = mode;
  notify(DEFAULT, mode_text);
}


//==========================================================================
//
//  close_file
//
//==========================================================================
void close_file (gboolean keep_monitor) {
  abort_smooth_scrolling();
  if (!Zathura.PDF.document) return;

  /* clean up pages */
  for (int i = 0; i < Zathura.PDF.number_of_pages; ++i) {
    Page *current_page = Zathura.PDF.pages[i];
    g_object_unref(current_page->page);
    if (current_page->label) g_free(current_page->label);
    free(current_page);
  }

  /* save bookmarks */
  if (Zathura.Bookmarks.data) {
    read_bookmarks_file();
    /* remove current file section, so it will come last (for history) */
    GError *err = NULL;
    err = NULL;
    g_key_file_remove_group(Zathura.Bookmarks.data, Zathura.PDF.file, &err);
    if (err) g_error_free(err);
    if (save_position) {
      /* set current page */
      g_key_file_set_integer(Zathura.Bookmarks.data, Zathura.PDF.file, bm_reserved_names[BM_PAGE_ENTRY], Zathura.PDF.page_number);
      /* set page offset */
      g_key_file_set_double(Zathura.Bookmarks.data, Zathura.PDF.file, bm_reserved_names[BM_PAGE_OFFSET], Zathura.PDF.page_yskip_t+Zathura.PDF.page_yskip_pix);
    }
    if (save_zoom_level) {
      /* set zoom level */
      g_key_file_set_integer(Zathura.Bookmarks.data, Zathura.PDF.file, bm_reserved_names[BM_PAGE_SCALE], Zathura.PDF.scale);
    }
    /* save scroll mode */
    if (Zathura.PDF.invert_page_scroll_mode) {
      g_key_file_set_integer(Zathura.Bookmarks.data, Zathura.PDF.file, bm_reserved_names[BM_PAGE_SCROLL_MODE], Zathura.PDF.invert_page_scroll_mode);
    }
    /* save offsets */
    g_key_file_set_integer(Zathura.Bookmarks.data, Zathura.PDF.file, bm_reserved_names[BM_XCENTER_OFS], Zathura.PDF.xcenter_offset);
    g_key_file_set_integer(Zathura.Bookmarks.data, Zathura.PDF.file, bm_reserved_names[BM_XLEFT_OFS], Zathura.PDF.xleft_offset);
    g_key_file_set_integer(Zathura.Bookmarks.data, Zathura.PDF.file, bm_reserved_names[BM_XRIGHT_OFS], Zathura.PDF.xright_offset);

    write_bookmarks_file();
    free_bookmarks();
  }

  /* inotify */
  if (!keep_monitor) {
    g_object_unref(Zathura.FileMonitor.monitor);
    Zathura.FileMonitor.monitor = NULL;
    if (Zathura.FileMonitor.file) {
      g_object_unref(Zathura.FileMonitor.file);
      Zathura.FileMonitor.file = NULL;
    }
  }

  /* clear search */
  if (Zathura.Search.query) g_free(Zathura.Search.query);
  Zathura.Search.active = FALSE;
  Zathura.Search.query = NULL;

  /* reset values */
  g_free(Zathura.PDF.pages);
  g_object_unref(Zathura.PDF.document);
  g_free(Zathura.State.pages);
  gtk_window_set_title(GTK_WINDOW(Zathura.UI.window), "zathura");

  Zathura.State.pages = g_strdup("");
  g_free(Zathura.State.filename);
  Zathura.State.filename = g_strdup((char *)default_text);

  Zathura.PDF.document = NULL;

  if (!keep_monitor) {
    g_free(Zathura.PDF.file);
    g_free(Zathura.PDF.password);
    Zathura.PDF.file = NULL;
    Zathura.PDF.password = NULL;
    Zathura.PDF.page_number = 0;
    Zathura.PDF.scale = 0;
    Zathura.PDF.rotate = 0;
  }
  Zathura.PDF.number_of_pages = 0;
  Zathura.PDF.page_yskip_t = 0;
  Zathura.PDF.page_yskip_pix = 0;
  Zathura.PDF.page_gap = 8;
  Zathura.PDF.invert_page_scroll_mode = 0;
  Zathura.PDF.xcenter_offset = 0;
  Zathura.PDF.xleft_offset = 0;
  Zathura.PDF.xright_offset = 0;

  surfCacheDestroy();

  /* destroy index */
  if (Zathura.UI.index) {
    gtk_widget_destroy(Zathura.UI.index);
    Zathura.UI.index = NULL;
  }

  /* destroy information */
  if (Zathura.UI.information) {
    gtk_widget_destroy(Zathura.UI.information);
    Zathura.UI.information = NULL;
  }

  /* free markers */
  if (Zathura.Marker.markers) free(Zathura.Marker.markers);
  Zathura.Marker.number_of_markers =  0;
  Zathura.Marker.last = -1;

  update_status();
}


//==========================================================================
//
//  enter_password
//
//==========================================================================
void enter_password (void) {
  /* replace default inputbar handler */
  g_signal_handler_disconnect((gpointer) Zathura.UI.inputbar, Zathura.Handler.inputbar_activate);
  Zathura.Handler.inputbar_activate = g_signal_connect(G_OBJECT(Zathura.UI.inputbar), "activate", G_CALLBACK(cb_inputbar_password_activate), NULL);

  Argument argument;
  argument.data = "Enter password: ";
  sc_focus_inputbar(&argument);
}


//==========================================================================
//
//  eval_marker
//
//==========================================================================
void eval_marker (int id) {
  /* go to last marker */
  if (id == 0x27) {
    int current_page = Zathura.PDF.page_number;
    set_page(Zathura.Marker.last);
    Zathura.Marker.last = current_page;
    return;
  }
  /* search markers */
  for (int i = 0; i < Zathura.Marker.number_of_markers; ++i) {
    if (Zathura.Marker.markers[i].id == id) {
      set_page(Zathura.Marker.markers[i].page);
      return;
    }
  }
}


//==========================================================================
//
//  highlight_result
//
//==========================================================================
void highlight_result (int page_id, PopplerRectangle *rectangle) {
  PopplerRectangle *trect = poppler_rectangle_copy(rectangle);
  cairo_t *cairo = cairo_create(create_page_surface(page_id));
  cairo_set_source_rgba(cairo, Zathura.Style.search_highlight.red, Zathura.Style.search_highlight.green, Zathura.Style.search_highlight.blue, transparency);

  recalc_rectangle(page_id, trect);
  cairo_rectangle(cairo, trect->x1, trect->y1, trect->x2-trect->x1, trect->y2-trect->y1);
  poppler_rectangle_free(trect);
  cairo_fill(cairo);
  cairo_destroy(cairo);
}


//==========================================================================
//
//  notify
//
//==========================================================================
void notify (int level, const char *message) {
  switch(level) {
    case ERROR:
      gtk_widget_modify_base(GTK_WIDGET(Zathura.UI.inputbar), GTK_STATE_NORMAL, &(Zathura.Style.notification_e_bg));
      gtk_widget_modify_text(GTK_WIDGET(Zathura.UI.inputbar), GTK_STATE_NORMAL, &(Zathura.Style.notification_e_fg));
      break;
    case WARNING:
      gtk_widget_modify_base(GTK_WIDGET(Zathura.UI.inputbar), GTK_STATE_NORMAL, &(Zathura.Style.notification_w_bg));
      gtk_widget_modify_text(GTK_WIDGET(Zathura.UI.inputbar), GTK_STATE_NORMAL, &(Zathura.Style.notification_w_fg));
      break;
    default:
      gtk_widget_modify_base(GTK_WIDGET(Zathura.UI.inputbar), GTK_STATE_NORMAL, &(Zathura.Style.inputbar_bg));
      gtk_widget_modify_text(GTK_WIDGET(Zathura.UI.inputbar), GTK_STATE_NORMAL, &(Zathura.Style.inputbar_fg));
      break;
  }
  if (message) gtk_entry_set_text(Zathura.UI.inputbar, message);
}


//==========================================================================
//
//  open_file
//
//==========================================================================
gboolean open_file (char *path, char *password) {
  abort_smooth_scrolling();

  /* specify path max */
  size_t pm;
#ifdef PATH_MAX
  pm = PATH_MAX;
#else
  pm = pathconf(path, _PC_PATH_MAX);
  if (pm <= 0) pm = 4096;
#endif

  char *rpath = NULL;
  if (path[0] == '~') {
    gchar *home_path = get_home_dir();
    rpath = g_build_filename(home_path, path+1, NULL);
    g_free(home_path);
  } else {
    rpath = g_strdup(path);
  }

  /* get filename */
  char *file = (char *)g_malloc0(sizeof(char)*pm);
  if (!file) out_of_memory();

  if (!realpath(rpath, file)) {
    notify(ERROR, "File does not exist");
    g_free(file);
    g_free(rpath);
    return FALSE;
  }
  g_free(rpath);

  /* check if file exists */
  if (!g_file_test(file, G_FILE_TEST_IS_REGULAR)) {
    notify(ERROR, "File does not exist");
    g_free(file);
    return FALSE;
  }

  /* close old file */
  close_file(FALSE);

  /* format path */
  GError *error = NULL;
  char *file_uri = g_filename_to_uri(file, NULL, &error);
  if (!file_uri) {
    if (file) g_free(file);
    char *message = g_strdup_printf("Can not open file: %s", error->message);
    notify(ERROR, message);
    g_free(message);
    g_error_free(error);
    return FALSE;
  }

  /* open file */
  Zathura.PDF.document = poppler_document_new_from_file(file_uri, password, &error);

  if (!Zathura.PDF.document) {
    if (error->code == 1) {
      g_free(file_uri);
      g_error_free(error);
      Zathura.PDF.file = file;
      enter_password();
      return FALSE;
    } else {
      char *message = g_strdup_printf("Can not open file: %s", error->message);
      notify(ERROR, message);
      g_free(file_uri);
      g_free(message);
      g_error_free(error);
      return FALSE;
    }
  }

  /* save password */
  g_free(Zathura.PDF.password);
  Zathura.PDF.password = (password ? g_strdup(password) : NULL);

  /* inotify */
  if (!Zathura.FileMonitor.monitor) {
    GFile *file = g_file_new_for_uri(file_uri);
    if (file) {
      Zathura.FileMonitor.monitor = g_file_monitor_file(file, G_FILE_MONITOR_NONE, NULL, NULL);
      if (Zathura.FileMonitor.monitor) g_signal_connect(G_OBJECT(Zathura.FileMonitor.monitor), "changed", G_CALLBACK(cb_watch_file), NULL);
      Zathura.FileMonitor.file = file;
    }
  }

  g_free(file_uri);

  Zathura.PDF.number_of_pages = poppler_document_get_n_pages(Zathura.PDF.document);
  g_free(Zathura.PDF.file);
  Zathura.PDF.file = file;
  Zathura.PDF.scale = 100;
  Zathura.PDF.rotate = 0;
  Zathura.PDF.page_gap = 8;
  Zathura.PDF.invert_page_scroll_mode = 0;
  Zathura.PDF.xcenter_offset = 0;
  Zathura.PDF.xleft_offset = 0;
  Zathura.PDF.xright_offset = 0;
  if (Zathura.State.filename) g_free(Zathura.State.filename);
  Zathura.State.filename = g_markup_escape_text(file, -1);
  Zathura.PDF.pages = g_malloc(Zathura.PDF.number_of_pages*sizeof(Page *));
  surfCacheDestroy();

  if (Zathura.Search.query) g_free(Zathura.Search.query);
  Zathura.Search.active = FALSE;
  Zathura.Search.query = NULL;

  if (!Zathura.PDF.pages) out_of_memory();

  /* get pages and check label mode */
  Zathura.Global.enable_labelmode = FALSE;

  char *errmsg = NULL;
  printf("loading %d pages...\n", Zathura.PDF.number_of_pages);
  for (int i = 0; i < Zathura.PDF.number_of_pages; ++i) {
    //printf("  ...page #%d\n", i+1);
    Zathura.PDF.pages[i] = malloc(sizeof(Page));
    if (!Zathura.PDF.pages[i]) out_of_memory();

    Zathura.PDF.pages[i]->id = i+1;
    Zathura.PDF.pages[i]->page = poppler_document_get_page(Zathura.PDF.document, i);
    if (!Zathura.PDF.pages[i]->page) {
      fprintf(stderr, "WARNING! can't load page #%d of %d...\n", i+1, Zathura.PDF.number_of_pages);
      // abort here, it seems that we're out of memory (or poppler failed)
      free(Zathura.PDF.pages[i]);
      Zathura.PDF.pages[i] = NULL;
      errmsg = g_strdup_printf("Aborted loading on page #%d of %d due to errors", i+1, Zathura.PDF.number_of_pages);
      Zathura.PDF.number_of_pages = i;
      break;
    }
    //printf("  ...page #%d: %p\n", i+1, Zathura.PDF.pages[i]->page);
    g_object_get(G_OBJECT(Zathura.PDF.pages[i]->page), "label", &(Zathura.PDF.pages[i]->label), NULL);

    /* check if it is necessary to use the label mode */
    int label_int = atoi(Zathura.PDF.pages[i]->label);
    if (label_int == 0 || label_int != i+1) Zathura.Global.enable_labelmode = TRUE;

    poppler_page_get_size(Zathura.PDF.pages[i]->page, &Zathura.PDF.pages[i]->width, &Zathura.PDF.pages[i]->height);
    if (Zathura.PDF.pages[i]->width < 1) Zathura.PDF.pages[i]->width = 1;
    if (Zathura.PDF.pages[i]->height < 1) Zathura.PDF.pages[i]->height = 1;
    //fprintf(stderr, "PAGE #%d: size=(%gx%g)\n", i, Zathura.PDF.pages[i]->width, Zathura.PDF.pages[i]->height);
  }

  /* set correct goto mode */
  if (!Zathura.Global.enable_labelmode && GOTO_MODE == GOTO_LABELS) Zathura.Global.goto_mode = GOTO_DEFAULT;

  /* start page */
  int start_page = 0;
  Zathura.PDF.page_yskip_t = 0;
  Zathura.PDF.page_yskip_pix = 0;

  /* bookmarks */
  if (Zathura.Bookmarks.data && g_key_file_has_group(Zathura.Bookmarks.data, file)) {
    /* get last opened page */
    if (save_position && g_key_file_has_key(Zathura.Bookmarks.data, file, bm_reserved_names[BM_PAGE_ENTRY], NULL)) {
      start_page = g_key_file_get_integer(Zathura.Bookmarks.data, file, bm_reserved_names[BM_PAGE_ENTRY], NULL);
    }
    /* get page offset */
    if (save_position && g_key_file_has_key(Zathura.Bookmarks.data, file, bm_reserved_names[BM_PAGE_OFFSET], NULL)) {
      double ofs = g_key_file_get_double(Zathura.Bookmarks.data, file, bm_reserved_names[BM_PAGE_OFFSET], NULL);
      if (ofs < 0) ofs = 0;
      int px = trunc(ofs);
      ofs -= px;
      if (px > Zathura.PDF.page_gap) px = Zathura.PDF.page_gap;
      Zathura.PDF.page_yskip_t = ofs;
      Zathura.PDF.page_yskip_pix = px;
    }
    /* get zoom level */
    if (save_zoom_level && g_key_file_has_key(Zathura.Bookmarks.data, file, bm_reserved_names[BM_PAGE_SCALE], NULL)) {
      Zathura.PDF.scale = g_key_file_get_integer(Zathura.Bookmarks.data, file, bm_reserved_names[BM_PAGE_SCALE], NULL);
      Zathura.Global.adjust_mode = ADJUST_NONE;
    }
    if (Zathura.PDF.scale > zoom_max) Zathura.PDF.scale = zoom_max;
    if (Zathura.PDF.scale < zoom_min) Zathura.PDF.scale = zoom_min;
    /* get page scroll inversion */
    if (g_key_file_has_key(Zathura.Bookmarks.data, file, bm_reserved_names[BM_PAGE_SCROLL_MODE], NULL)) {
      Zathura.PDF.invert_page_scroll_mode = g_key_file_get_integer(Zathura.Bookmarks.data, file, bm_reserved_names[BM_PAGE_SCROLL_MODE], NULL);
    }
    /* get x offsets */
    if (g_key_file_has_key(Zathura.Bookmarks.data, file, bm_reserved_names[BM_XCENTER_OFS], NULL)) {
      Zathura.PDF.xcenter_offset = g_key_file_get_integer(Zathura.Bookmarks.data, file, bm_reserved_names[BM_XCENTER_OFS], NULL);
    }
    if (g_key_file_has_key(Zathura.Bookmarks.data, file, bm_reserved_names[BM_XLEFT_OFS], NULL)) {
      Zathura.PDF.xleft_offset = g_key_file_get_integer(Zathura.Bookmarks.data, file, bm_reserved_names[BM_XLEFT_OFS], NULL);
    }
    if (g_key_file_has_key(Zathura.Bookmarks.data, file, bm_reserved_names[BM_XRIGHT_OFS], NULL)) {
      Zathura.PDF.xright_offset = g_key_file_get_integer(Zathura.Bookmarks.data, file, bm_reserved_names[BM_XRIGHT_OFS], NULL);
    }

    /* open and read bookmark file */
    gsize number_of_keys = 0;
    char **keys = g_key_file_get_keys(Zathura.Bookmarks.data, file, &number_of_keys, NULL);

    for (gsize i = 0; i < number_of_keys; ++i) {
      if (!is_reserved_bm_name(keys[i])) {
        Zathura.Bookmarks.bookmarks = safe_realloc((void **)&Zathura.Bookmarks.bookmarks, Zathura.Bookmarks.number_of_bookmarks+1, sizeof(Bookmark));
        if (!Zathura.Bookmarks.bookmarks) out_of_memory();

        Zathura.Bookmarks.bookmarks[Zathura.Bookmarks.number_of_bookmarks].id = g_strdup(keys[i]);
        Zathura.Bookmarks.bookmarks[Zathura.Bookmarks.number_of_bookmarks].page = g_key_file_get_integer(Zathura.Bookmarks.data, file, keys[i], NULL);

        ++Zathura.Bookmarks.number_of_bookmarks;
      }
    }

    g_strfreev(keys);
  }

  /* set window title */
  gtk_window_set_title(GTK_WINDOW(Zathura.UI.window), basename(file));

  /* show document */
  set_page_keep_ofs(start_page);
  update_status();

  isc_abort(NULL);

  if (errmsg) {
    notify(WARNING, errmsg);
    g_free(errmsg);
  }

  return TRUE;
}


//==========================================================================
//
//  open_stdin
//
//==========================================================================
gboolean open_stdin (gchar *password) {
  GError *error = NULL;
  gchar *file = NULL;
  gint handle = g_file_open_tmp("zathura.stdin.XXXXXX.pdf", &file, &error);
  if (handle == -1) {
    gchar *message = g_strdup_printf("Can not create temporary file: %s", error->message);
    notify(ERROR, message);
    g_free(message);
    g_error_free(error);
    return FALSE;
  }

  // read from stdin and dump to temporary file
  int stdinfno = fileno(stdin);
  if (stdinfno == -1) {
    gchar *message = g_strdup_printf("Can not read from stdin.");
    notify(ERROR, message);
    g_free(message);
    close(handle);
    g_unlink(file);
    g_free(file);
    return FALSE;

  }

  char buffer[BUFSIZ];
  ssize_t count = 0;
  while ((count = read(stdinfno, buffer, BUFSIZ)) > 0) {
    if (write(handle, buffer, count) != count) {
      gchar *message = g_strdup_printf("Can not write to temporary file: %s", file);
      notify(ERROR, message);
      g_free(message);
      close(handle);
      g_unlink(file);
      g_free(file);
      return FALSE;
    }
  }
  close(handle);

  if (count != 0) {
    gchar *message = g_strdup_printf("Can not read from stdin.");
    notify(ERROR, message);
    g_free(message);
    g_unlink(file);
    g_free(file);
    return FALSE;
  }

  /* update data */
  if (Zathura.StdinSupport.file) g_unlink(Zathura.StdinSupport.file);
  g_free(Zathura.StdinSupport.file);
  Zathura.StdinSupport.file = file;

  return open_file(Zathura.StdinSupport.file, password);
}


//==========================================================================
//
//  open_uri
//
//==========================================================================
void open_uri (char *uri) {
  char *escaped_uri = g_shell_quote(uri);
  char *uri_cmd = g_strdup_printf(uri_command, escaped_uri);
  system(uri_cmd);
  g_free(uri_cmd);
  g_free(escaped_uri);
}


//==========================================================================
//
//  out_of_memory
//
//==========================================================================
void out_of_memory (void) {
  printf("error: out of memory\n");
  *(char*)0 = 0;
  exit(-1);
}


//==========================================================================
//
//  update_status
//
//==========================================================================
void update_status (void) {
  /* update text */
  gtk_label_set_markup((GtkLabel *)Zathura.Global.status_text, Zathura.State.filename);
  /* update pages */
  if (Zathura.PDF.document && Zathura.PDF.pages) {
    int page = Zathura.PDF.page_number;
    g_free(Zathura.State.pages);
    Zathura.State.pages = g_strdup_printf("[%i/%i]", page+1, Zathura.PDF.number_of_pages);
  }
  /* update state */
  char *zoom_level = (Zathura.PDF.scale != 0 ? g_strdup_printf("%d%%", Zathura.PDF.scale) : g_strdup(""));
  char *goto_mode = (Zathura.Global.goto_mode == GOTO_LABELS ? "L" : (Zathura.Global.goto_mode == GOTO_OFFSET ? "O" : "D"));
  char *status_text = g_strdup_printf("%s [%s] %s (%d%%)", zoom_level, goto_mode, Zathura.State.pages, Zathura.State.scroll_percentage);
  gtk_label_set_markup((GtkLabel *)Zathura.Global.status_state, status_text);
  g_free(status_text);
  g_free(zoom_level);
}


//==========================================================================
//
//  read_bookmarks_file
//
//  this also removes groups that has no corresponding file
//
//==========================================================================
void read_bookmarks_file (void) {
  /* free it at first */
  if (Zathura.Bookmarks.data) g_key_file_free(Zathura.Bookmarks.data);
  /* create or open existing bookmark file */
  Zathura.Bookmarks.data = g_key_file_new();
  if (!g_file_test(Zathura.Bookmarks.file, G_FILE_TEST_IS_REGULAR)) {
    /* file does not exist */
    gchar *s = g_strdup_printf("# Zathura bookmarks\n\n[%s]\n%s=0\n", main_group_name, lastcheck_key_name);
    g_file_set_contents(Zathura.Bookmarks.file, s, -1, NULL);
    g_free(s);
  }
  GError *error = NULL;
  if (!g_key_file_load_from_file(Zathura.Bookmarks.data, Zathura.Bookmarks.file, G_KEY_FILE_KEEP_COMMENTS|G_KEY_FILE_KEEP_TRANSLATIONS, &error)) {
    gchar *message = g_strdup_printf("Could not load bookmark file: %s", error->message);
    notify(ERROR, message);
    g_free(message);
  }
}


//==========================================================================
//
//  write_bookmarks_file
//
//==========================================================================
void write_bookmarks_file (void) {
  if (!Zathura.Bookmarks.data) return; /* nothing to do */

  /* scan groups, and remove groups for which we don't have a file anymore */
  GError *err = NULL;
  double lastcheck = 0;
  if (g_key_file_has_key(Zathura.Bookmarks.data, main_group_name, lastcheck_key_name, &err)) {
    g_key_file_get_double(Zathura.Bookmarks.data, main_group_name, lastcheck_key_name, &err);
  }
  if (err) g_error_free(err);
  const double currtime = sys_time();
  /* check once a week */
  const int scantime = 60*60*24*7;
  if (lastcheck+scantime <= currtime /*|| true*/) {
    gchar **groups = g_key_file_get_groups(Zathura.Bookmarks.data, NULL);
    if (groups) {
      for (gchar **grp = groups; *grp; ++grp) {
        gchar *gname = *grp;
        if (strcmp(gname, main_group_name) != 0 && access(gname, R_OK) != 0) {
          //fprintf(stderr, "***<%s>\n", gname);
          err = NULL;
          g_key_file_remove_group(Zathura.Bookmarks.data, gname, &err);
          if (err) g_error_free(err);
        }
      }
      g_strfreev(groups);
    }
    /* save check time */
    g_key_file_set_double(Zathura.Bookmarks.data, main_group_name, lastcheck_key_name, currtime);
  }

  /* save bookmarks */
  for (int i = 0; i < Zathura.Bookmarks.number_of_bookmarks; ++i) {
    g_key_file_set_integer(Zathura.Bookmarks.data, Zathura.PDF.file, Zathura.Bookmarks.bookmarks[i].id, Zathura.Bookmarks.bookmarks[i].page);
  }

  /* convert file and save it */
  gchar *bookmarks = g_key_file_to_data(Zathura.Bookmarks.data, NULL, NULL);
  g_file_set_contents(Zathura.Bookmarks.file, bookmarks, -1, NULL);
  g_free(bookmarks);
}


//==========================================================================
//
//  free_bookmarks
//
//==========================================================================
void free_bookmarks (void) {
  for (int i = 0; i < Zathura.Bookmarks.number_of_bookmarks; ++i) g_free(Zathura.Bookmarks.bookmarks[i].id);
  free(Zathura.Bookmarks.bookmarks);
  Zathura.Bookmarks.bookmarks = NULL;
  Zathura.Bookmarks.number_of_bookmarks = 0;
}


//==========================================================================
//
//  read_configuration_file
//
//==========================================================================
void read_configuration_file (const char *rcfile) {
  if (!rcfile) return;
  if (!g_file_test(rcfile, G_FILE_TEST_IS_REGULAR)) return;
  char *content = NULL;
  if (g_file_get_contents(rcfile, &content, NULL, NULL)) {
    gchar **lines = g_strsplit(content, "\n", -1);
    int n = g_strv_length(lines)-1;
    for (int i = 0; i <= n; ++i) {
      if (!strlen(lines[i])) continue;
      gchar **pre_tokens = g_strsplit_set(lines[i], "\t ", -1);
      int pre_length = g_strv_length(pre_tokens);
      gchar** tokens = g_malloc0(sizeof(gchar*)*(pre_length+1));
      gchar** tokp = tokens;
      int length = 0;
      for (int f = 0; f != pre_length; ++f) {
        if (strlen(pre_tokens[f])) {
          *tokp++ = pre_tokens[f];
          ++length;
        }
      }
      if (!strcmp(tokens[0], "set")) cmd_set(length-1, tokens+1);
      else if (!strcmp(tokens[0], "map")) cmd_map(length-1, tokens+1);
      g_free(tokens);
    }
    g_strfreev(lines);
    g_free(content);
  }
}


//==========================================================================
//
//  read_configuration
//
//==========================================================================
void read_configuration (void) {
  char *zathurarc = g_build_filename(Zathura.Config.config_dir, ZATHURA_RC, NULL);
  read_configuration_file(GLOBAL_RC);
  read_configuration_file(zathurarc);
  g_free(zathurarc);
}


//==========================================================================
//
//  recalc_rectangle
//
//==========================================================================
void recalc_rectangle (int page_id, PopplerRectangle* rectangle) {
  double x1 = rectangle->x1;
  double x2 = rectangle->x2;
  double y1 = rectangle->y1;
  double y2 = rectangle->y2;
  Page *current_page = Zathura.PDF.pages[page_id];
  const double page_width = current_page->width;
  const double page_height = current_page->height;

  const double scale = ((double) Zathura.PDF.scale/100.0);
  const int rotate = Zathura.PDF.rotate;
  switch (rotate) {
    case 90:
      rectangle->x1 = y2*scale;
      rectangle->y1 = x1*scale;
      rectangle->x2 = y1*scale;
      rectangle->y2 = x2*scale;
      break;
    case 180:
      rectangle->x1 = (page_width-x2)*scale;
      rectangle->y1 = y2*scale;
      rectangle->x2 = (page_width-x1)*scale;
      rectangle->y2 = y1*scale;
      break;
    case 270:
      rectangle->x1 = (page_height-y1)*scale;
      rectangle->y1 = (page_width-x2)*scale;
      rectangle->x2 = (page_height-y2)*scale;
      rectangle->y2 = (page_width-x1)*scale;
      break;
    default:
      rectangle->x1 = x1*scale;
      rectangle->y1 = (page_height-y1)*scale;
      rectangle->x2 = x2*scale;
      rectangle->y2 = (page_height-y2)*scale;
      break;
  }
}


//==========================================================================
//
//  create_completion_row
//
//==========================================================================
GtkEventBox *create_completion_row (GtkBox *results, char *command, char *description, gboolean group) {
  GtkBox *col = GTK_BOX(gtk_hbox_new(FALSE, 0));
  GtkEventBox *row = GTK_EVENT_BOX(gtk_event_box_new());
  GtkLabel *show_command = GTK_LABEL(gtk_label_new(NULL));
  GtkLabel *show_description = GTK_LABEL(gtk_label_new(NULL));
  gtk_misc_set_alignment(GTK_MISC(show_command), 0.0, 0.0);
  gtk_misc_set_alignment(GTK_MISC(show_description), 0.0, 0.0);
  if (group) {
    gtk_misc_set_padding(GTK_MISC(show_command), 2.0, 4.0);
    gtk_misc_set_padding(GTK_MISC(show_description), 2.0, 4.0);
  } else {
    gtk_misc_set_padding(GTK_MISC(show_command), 1.0, 1.0);
    gtk_misc_set_padding(GTK_MISC(show_description), 1.0, 1.0);
  }
  gtk_label_set_use_markup(show_command, TRUE);
  gtk_label_set_use_markup(show_description, TRUE);
  gchar *c = g_markup_printf_escaped(FORMAT_COMMAND, (command ? command : ""));
  gchar *d = g_markup_printf_escaped(FORMAT_DESCRIPTION, (description ? description : ""));
  gtk_label_set_markup(show_command, c);
  gtk_label_set_markup(show_description, d);
  g_free(c);
  g_free(d);
  if (group) {
    gtk_widget_modify_fg(GTK_WIDGET(show_command), GTK_STATE_NORMAL, &(Zathura.Style.completion_g_fg));
    gtk_widget_modify_fg(GTK_WIDGET(show_description), GTK_STATE_NORMAL, &(Zathura.Style.completion_g_fg));
    gtk_widget_modify_bg(GTK_WIDGET(row), GTK_STATE_NORMAL, &(Zathura.Style.completion_g_bg));
  } else {
    gtk_widget_modify_fg(GTK_WIDGET(show_command), GTK_STATE_NORMAL, &(Zathura.Style.completion_fg));
    gtk_widget_modify_fg(GTK_WIDGET(show_description), GTK_STATE_NORMAL, &(Zathura.Style.completion_fg));
    gtk_widget_modify_bg(GTK_WIDGET(row), GTK_STATE_NORMAL, &(Zathura.Style.completion_bg));
  }
  gtk_widget_modify_font(GTK_WIDGET(show_command), Zathura.Style.font);
  gtk_widget_modify_font(GTK_WIDGET(show_description), Zathura.Style.font);
  gtk_box_pack_start(GTK_BOX(col), GTK_WIDGET(show_command), TRUE,  TRUE, 2);
  gtk_box_pack_start(GTK_BOX(col), GTK_WIDGET(show_description), FALSE, FALSE, 2);
  gtk_container_add(GTK_CONTAINER(row), GTK_WIDGET(col));
  gtk_box_pack_start(results, GTK_WIDGET(row), FALSE, FALSE, 0);
  return row;
}


//==========================================================================
//
//  set_completion_row_color
//
//==========================================================================
void set_completion_row_color (GtkBox *results, int mode, int id) {
  GtkEventBox *row = (GtkEventBox *)g_list_nth_data(gtk_container_get_children(GTK_CONTAINER(results)), id);
  if (row) {
    GtkBox *col = (GtkBox *)g_list_nth_data(gtk_container_get_children(GTK_CONTAINER(row)), 0);
    GtkLabel *cmd = (GtkLabel *)g_list_nth_data(gtk_container_get_children(GTK_CONTAINER(col)), 0);
    GtkLabel *cdesc = (GtkLabel *)g_list_nth_data(gtk_container_get_children(GTK_CONTAINER(col)), 1);
    if (mode == NORMAL) {
      gtk_widget_modify_fg(GTK_WIDGET(cmd), GTK_STATE_NORMAL, &(Zathura.Style.completion_fg));
      gtk_widget_modify_fg(GTK_WIDGET(cdesc), GTK_STATE_NORMAL, &(Zathura.Style.completion_fg));
      gtk_widget_modify_bg(GTK_WIDGET(row), GTK_STATE_NORMAL, &(Zathura.Style.completion_bg));
    } else {
      gtk_widget_modify_fg(GTK_WIDGET(cmd), GTK_STATE_NORMAL, &(Zathura.Style.completion_hl_fg));
      gtk_widget_modify_fg(GTK_WIDGET(cdesc), GTK_STATE_NORMAL, &(Zathura.Style.completion_hl_fg));
      gtk_widget_modify_bg(GTK_WIDGET(row), GTK_STATE_NORMAL, &(Zathura.Style.completion_hl_bg));
    }
  }
}


//==========================================================================
//
//  set_page_keep_ofs
//
//  used in file open function to set the page, but don't reset the offsets
//
//==========================================================================
void set_page_keep_ofs (int page) {
  if (page >= Zathura.PDF.number_of_pages || page < 0) {
    notify(WARNING, "Could not open page");
    return;
  }
  Zathura.PDF.page_number = page;
  switch_view(Zathura.UI.document);
  draw(page);
}


//==========================================================================
//
//  set_page
//
//==========================================================================
void set_page (int page) {
  if (page >= Zathura.PDF.number_of_pages || page < 0) {
    notify(WARNING, "Could not open page");
    return;
  }
  Zathura.PDF.page_number = page;
  Zathura.PDF.page_yskip_t = 0;
  Zathura.PDF.page_yskip_pix = 0;
  Argument argument;
  argument.n = TOP;
  switch_view(Zathura.UI.document);
  draw(page);
  sc_scroll(&argument);
}


//==========================================================================
//
//  switch_view
//
//==========================================================================
void switch_view (GtkWidget *widget) {
  GtkWidget *child = gtk_bin_get_child(GTK_BIN(Zathura.UI.viewport));
  if (child == widget) return;
  if (child) {
    g_object_ref(child);
    gtk_container_remove(GTK_CONTAINER(Zathura.UI.viewport), child);
  }
  gtk_container_add(GTK_CONTAINER(Zathura.UI.viewport), GTK_WIDGET(widget));
}


//==========================================================================
//
//  completion_init
//
//==========================================================================
Completion *completion_init (void) {
  Completion *completion = malloc(sizeof(Completion));
  if (!completion) out_of_memory();
  completion->groups = NULL;
  return completion;
}


//==========================================================================
//
//  completion_group_create
//
//==========================================================================
CompletionGroup *completion_group_create (char *name) {
  CompletionGroup *group = malloc(sizeof(CompletionGroup));
  if (!group) out_of_memory();
  group->value = (name ? g_strdup(name) : NULL);
  group->elements = NULL;
  group->next = NULL;
  return group;
}


//==========================================================================
//
//  completion_add_group
//
//==========================================================================
void completion_add_group (Completion *completion, CompletionGroup *group) {
  CompletionGroup *cg = completion->groups;
  while (cg && cg->next) cg = cg->next;
  if (cg) cg->next = group; else completion->groups = group;
}


//==========================================================================
//
//  completion_free
//
//==========================================================================
void completion_free (Completion *completion) {
  CompletionGroup *group = completion->groups;
  while (group) {
    CompletionGroup *ng;
    CompletionElement *element = group->elements;
    while (element) {
      CompletionElement *ne = element->next;
      g_free(element->value);
      g_free(element->description);
      free(element);
      element = ne;
    }
    ng = group->next;
    g_free(group->value);
    free(group);
    group = ng;
  }
  free(completion);
}


//==========================================================================
//
//  completion_group_add_element
//
//==========================================================================
void completion_group_add_element (CompletionGroup *group, char *name, char *description) {
  CompletionElement *el = group->elements;
  while (el && el->next) el = el->next;
  CompletionElement *new_element = malloc(sizeof(CompletionElement));
  if (!new_element) out_of_memory();
  new_element->value = (name ? g_strdup(name) : NULL);
  new_element->description = (description ? g_strdup(description) : NULL);
  new_element->next = NULL;
  if (el) el->next = new_element; else group->elements = new_element;
}


//==========================================================================
//
//  completion_group_prepend_element
//
//==========================================================================
void completion_group_prepend_element (CompletionGroup *group, char *name, char *description) {
  CompletionElement *new_element = malloc(sizeof(CompletionElement));
  if (!new_element) out_of_memory();
  new_element->value = (name ? g_strdup(name) : NULL);
  new_element->description = (description ? g_strdup(description) : NULL);
  new_element->next = group->elements;
  group->elements = new_element;
}


//==========================================================================
//
//  completion_group_limit_last
//
//==========================================================================
void completion_group_limit_last (CompletionGroup *group, int count) {
  if (!group) return;
  if (count < 0) count = 0;
  if (count == 0) {
    /* clear group */
    CompletionElement *element = group->elements;
    while (element) {
      CompletionElement *ne = element->next;
      g_free(element->value);
      g_free(element->description);
      free(element);
      element = ne;
    }
    group->elements = NULL;
    return;
  }
  /* count elements */
  int total = 0;
  for (CompletionElement *e = group->elements; e; e = e->next) ++total;
  if (total <= count) return; /* nothing to do */
  /* remove first elements */
  total -= count; /* how much */
  while (total-- > 0 && group->elements) {
    CompletionElement *e = group->elements;
    group->elements = e->next;
    g_free(e->value);
    g_free(e->description);
    free(e);
  }
}


//==========================================================================
//
//  sc_abort
//
//  shortcut implementation
//
//==========================================================================
void sc_abort (Argument *argument) {
  /* Clear buffer */
  if (Zathura.Global.buffer) {
    g_string_free(Zathura.Global.buffer, TRUE);
    Zathura.Global.buffer = NULL;
    gtk_label_set_markup((GtkLabel *)Zathura.Global.status_buffer, "");
  }

  if (!Zathura.Global.show_inputbar) gtk_widget_hide(GTK_WIDGET(Zathura.UI.inputbar));

  /* Set back to normal mode */
  change_mode(NORMAL);
  switch_view(Zathura.UI.document);
}


//==========================================================================
//
//  sc_adjust_window
//
//==========================================================================
void sc_adjust_window (Argument *argument) {
  //fprintf(stderr, "RESIZED!\n");
  surfCacheDestroy(); // why not?

  if (!Zathura.PDF.document) return;
  Zathura.Global.adjust_mode = argument->n;
  GtkAdjustment *adjustment;
  double view_size;
  if (argument->n == ADJUST_BESTFIT) {
    adjustment = gtk_scrolled_window_get_vadjustment(Zathura.UI.view);
  } else if (argument->n == ADJUST_WIDTH) {
    adjustment = gtk_scrolled_window_get_hadjustment(Zathura.UI.view);
  } else {
    return;
  }
  view_size = gtk_adjustment_get_page_size(adjustment);

  Page *current_page = Zathura.PDF.pages[Zathura.PDF.page_number];
  double page_width = current_page->width;
  double page_height = current_page->height;

  if (Zathura.PDF.rotate == 90 || Zathura.PDF.rotate == 270) {
    double swap = page_width;
    page_width = page_height;
    page_height = swap;
  }

  if (argument->n == ADJUST_BESTFIT) {
    // for best fit, calculate average page height
    if (Zathura.PDF.number_of_pages > 0) {
      double total = 0;
      for (int f = 0; f < Zathura.PDF.number_of_pages; ++f) {
        //allwdt += (Zathura.PDF.rotate == 90 || Zathura.PDF.rotate == 270 ? current_page->height : current_page->width);
        Page *cpg = Zathura.PDF.pages[f];
        total += (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? cpg->height : cpg->width);
      }
      total /= Zathura.PDF.number_of_pages;
      Zathura.PDF.scale = (view_size/total)*100;
    }
  } else {
    Zathura.PDF.scale = (view_size/page_width)*100;
  }

  draw(Zathura.PDF.page_number);
  update_status();
}


//==========================================================================
//
//  sc_xcenter_window
//
//==========================================================================
void sc_xcenter_window (Argument *argument) {
  if (!Zathura.PDF.document) return;
  Zathura.Global.xcenter_mode = argument->n;
  draw(Zathura.PDF.page_number);
  update_status();
}


//==========================================================================
//
//  sc_xoffset_modify
//
//==========================================================================
void sc_xoffset_modify (Argument *argument) {
  if (!Zathura.PDF.document) return;
  int *pofs;
  switch (Zathura.Global.xcenter_mode) {
    case CENTER_LEFT_TOP: pofs = &Zathura.PDF.xleft_offset; break;
    case CENTER_RIGHT_BOTTOM: pofs = &Zathura.PDF.xright_offset; break;
    default: pofs = &Zathura.PDF.xcenter_offset; break;
  }
  const int dir = (argument->n == XOFFSET_DECREMENT || argument->n == XOFFSET_DECREMENT_SLOW ? -1 : 1);
  const int speed = (argument->n == XOFFSET_DECREMENT_SLOW || argument->n == XOFFSET_INCREMENT_SLOW ? 8 : 32);
  *pofs += dir*speed;
  draw(Zathura.PDF.page_number);
  update_status();
}


//==========================================================================
//
//  sc_change_buffer
//
//==========================================================================
void sc_change_buffer (Argument *argument) {
  if (!Zathura.Global.buffer) return;
  int buffer_length = Zathura.Global.buffer->len;
  if (argument->n == DELETE_LAST) {
    if (buffer_length-1 == 0) {
      g_string_free(Zathura.Global.buffer, TRUE);
      Zathura.Global.buffer = NULL;
      gtk_label_set_markup((GtkLabel*) Zathura.Global.status_buffer, "");
    } else {
      GString *temp = g_string_new_len(Zathura.Global.buffer->str, buffer_length-1);
      g_string_free(Zathura.Global.buffer, TRUE);
      Zathura.Global.buffer = temp;
      gtk_label_set_markup((GtkLabel *)Zathura.Global.status_buffer, Zathura.Global.buffer->str);
    }
  }
}


//==========================================================================
//
//  sc_change_mode
//
//==========================================================================
void sc_change_mode (Argument *argument) {
  if (argument) change_mode(argument->n);
}


//==========================================================================
//
//  sc_focus_inputbar
//
//==========================================================================
void sc_focus_inputbar (Argument *argument) {
  if (!(GTK_WIDGET_VISIBLE(GTK_WIDGET(Zathura.UI.inputbar)))) gtk_widget_show(GTK_WIDGET(Zathura.UI.inputbar));
  if (argument->data) {
    char *data;
    if (argument->n == APPEND_FILEPATH) {
      data = g_strdup_printf("%s%s", (const char *)argument->data, Zathura.PDF.file);
    } else {
      data = g_strdup((const char *)argument->data);
    }
    notify(DEFAULT, data);
    g_free(data);
    gtk_widget_grab_focus(GTK_WIDGET(Zathura.UI.inputbar));
    gtk_editable_set_position(GTK_EDITABLE(Zathura.UI.inputbar), -1);
    if (argument->n == OPEN_COMPLETION_LAST) {
      Argument arg = { LAST };
      isc_completion(&arg);
    }
  }
}


//==========================================================================
//
//  sc_follow
//
//==========================================================================
void sc_follow (Argument *argument) {
  if (!Zathura.PDF.document) return;
  Page *current_page = Zathura.PDF.pages[Zathura.PDF.page_number];
  int link_id = 1;

  GList *link_list = poppler_page_get_link_mapping(current_page->page);
  link_list = g_list_reverse(link_list);

  if (g_list_length(link_list) <= 0) return;

  for (GList *links = link_list; links; links = g_list_next(links)) {
    PopplerLinkMapping *link_mapping = (PopplerLinkMapping *)links->data;
    PopplerRectangle *link_rectangle = &link_mapping->area;
    PopplerAction *action = link_mapping->action;

    /* only handle URI and internal links */
    if (action->type == POPPLER_ACTION_URI || action->type == POPPLER_ACTION_GOTO_DEST) {
      highlight_result(Zathura.PDF.page_number, link_rectangle);
      /* draw text */
      recalc_rectangle(Zathura.PDF.page_number, link_rectangle);
      cairo_t *cairo = cairo_create(create_page_surface(Zathura.PDF.page_number));
      cairo_select_font_face(cairo, font, CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
      cairo_set_font_size(cairo, 10);
      cairo_move_to(cairo, link_rectangle->x1+1, link_rectangle->y1-1);
      char *link_number = g_strdup_printf("%i", link_id++);
      cairo_show_text(cairo, link_number);
      cairo_destroy(cairo);
      g_free(link_number);
    }
  }

  gtk_widget_queue_draw(Zathura.UI.drawing_area);
  poppler_page_free_link_mapping(link_list);

  /* replace default inputbar handler */
  g_signal_handler_disconnect((gpointer) Zathura.UI.inputbar, Zathura.Handler.inputbar_activate);
  Zathura.Handler.inputbar_activate = g_signal_connect(G_OBJECT(Zathura.UI.inputbar), "activate", G_CALLBACK(cb_inputbar_form_activate), NULL);

  argument->data = "Follow hint: ";
  sc_focus_inputbar(argument);
}


//==========================================================================
//
//  sc_navigate
//
//==========================================================================
void sc_navigate (Argument *argument) {
  if (!Zathura.PDF.document) return;

  int number_of_pages = Zathura.PDF.number_of_pages;
  int new_page = Zathura.PDF.page_number;

  if (argument->n == NEXT) {
    new_page = (scroll_wrap ? (new_page+1)%number_of_pages : new_page+1);
  } else if (argument->n == PREVIOUS) {
    new_page = (scroll_wrap ? (new_page+number_of_pages-1)%number_of_pages : new_page-1);
    if (new_page < 0 && Zathura.PDF.page_yskip_t > 0) new_page = 0;
  }
  if (!scroll_wrap && (new_page < 0 || new_page >= number_of_pages)) return;

  set_page(new_page);
  update_status();
}


//==========================================================================
//
//  sc_recolor
//
//==========================================================================
void sc_recolor (Argument *argument) {
  Zathura.Global.recolor = !Zathura.Global.recolor;
  draw(Zathura.PDF.page_number);
}


//==========================================================================
//
//  sc_reload
//
//==========================================================================
void sc_reload (Argument *argument) {
  draw(Zathura.PDF.page_number);

  GtkAdjustment *vadjustment = gtk_scrolled_window_get_vadjustment(Zathura.UI.view);
  GtkAdjustment *hadjustment = gtk_scrolled_window_get_hadjustment(Zathura.UI.view);

  /* save old information */
  char *path = (Zathura.PDF.file ? strdup(Zathura.PDF.file) : NULL);
  char *password = (Zathura.PDF.password ? strdup(Zathura.PDF.password) : NULL);
  int scale = Zathura.PDF.scale;
  int page = Zathura.PDF.page_number;
  int rotate = Zathura.PDF.rotate;
  gdouble va = 0; //gtk_adjustment_get_value(vadjustment);
  gdouble ha = 0; //gtk_adjustment_get_value(hadjustment);

  /* reopen and restore settings */
  close_file(TRUE);
  open_file(path, password);

  Zathura.PDF.scale = scale;
  Zathura.PDF.rotate = rotate;
  /*va = ha = 0;*/
  gtk_adjustment_set_value(vadjustment, va);
  gtk_adjustment_set_value(hadjustment, ha);

  if (Zathura.PDF.number_of_pages != 0) {
    if (page >= Zathura.PDF.number_of_pages-1) page = Zathura.PDF.number_of_pages-1;
    Zathura.PDF.page_number = page;
    draw(Zathura.PDF.page_number);
  }

  if (path) free(path);
  if (password) free(password);
}


//==========================================================================
//
//  sc_rotate
//
//==========================================================================
void sc_rotate (Argument *argument) {
  Zathura.PDF.rotate = (Zathura.PDF.rotate+90)%360;
  draw(Zathura.PDF.page_number);
}


//==========================================================================
//
//  abort_smooth_scrolling
//
//==========================================================================
void abort_smooth_scrolling (void) {
  if (Zathura.UI.scroll_timer_id > 0) {
    g_source_remove(Zathura.UI.scroll_timer_id);
    Zathura.UI.scroll_timer_id = 0;
  }
}


//==========================================================================
//
//  cb_scroll_timer
//
//==========================================================================
gint cb_scroll_timer (gpointer data) {
  int currypos = calc_current_document_screen_offset();
  int dest = Zathura.UI.scroll_destination;
  int delta = Zathura.UI.scroll_step;
  int lastypos = currypos;

  // if our destination is more than two screens away, go faster
  if (Zathura.UI.view) {
    GtkAdjustment *adjustment = gtk_scrolled_window_get_vadjustment(Zathura.UI.view);
    int window_y = gtk_adjustment_get_page_size(adjustment);
    if (abs(currypos-dest) >= window_y*2 && delta < window_y/2) {
      int n = window_y/2;
      //fprintf(stderr, "FASTENING! delta=%d; n=%d\n", delta, n);
      if (n > delta) delta = n;
    }
  }

  if (delta < 1) {
    currypos = dest;
  } else if (currypos < dest) {
    if ((currypos += delta) > dest) currypos = dest;
  } else if (currypos > dest) {
    if ((currypos -= delta) < dest) currypos = dest;
  }

  convert_screen_document_offset_to_pos(currypos, &Zathura.PDF.page_number, &Zathura.PDF.page_yskip_t, &Zathura.PDF.page_yskip_pix);

  render_view(Zathura.UI.drawing_area, FALSE); // no full clear
  update_status();

  // if not moved, means that scrolling is failed for some reason, so abort it
  if (dest == currypos || calc_current_document_screen_offset() == lastypos) {
    Zathura.UI.scroll_timer_id = 0;
    return FALSE;
  }

  return TRUE;
}


//==========================================================================
//
//  get_current_smooth_destination
//
//  <0: no smooth scrolling currently active
//
//==========================================================================
int get_current_smooth_destination (void) {
  if (Zathura.UI.scroll_timer_id <= 0) return -1;
  return Zathura.UI.scroll_destination;
}


//==========================================================================
//
//  setup_smooth_scroll_to
//
//==========================================================================
void setup_smooth_scroll_to (int destination, int delta) {
  if (!Zathura.PDF.document || delta < 1) {
    abort_smooth_scrolling();
    return;
  }

  int currypos = calc_current_document_screen_offset();
  if (currypos == destination) {
    abort_smooth_scrolling();
    return;
  }

  if ((destination < currypos && destination+delta >= currypos) ||
      (destination > currypos && destination-delta <= currypos))
  {
    // no need to scroll, simply jump there
    abort_smooth_scrolling();
    convert_screen_document_offset_to_pos(currypos, &Zathura.PDF.page_number, &Zathura.PDF.page_yskip_t, &Zathura.PDF.page_yskip_pix);
    render_view(Zathura.UI.drawing_area, FALSE); // no full clear
    update_status();
    return;
  }

  // setup scroll timer
  Zathura.UI.scroll_step = delta;
  Zathura.UI.scroll_destination = destination;

  // create scroll timer
  if (Zathura.UI.scroll_timer_id <= 0) {
    Zathura.UI.scroll_timer_id = g_timeout_add(5, &cb_scroll_timer, NULL);
    //fprintf(stderr, "added timer %d\n", Zathura.UI.scroll_timer_id);
  }

  // cache prev/next pages
  const int pgdelta = (destination < currypos ? -1 : 1);
  int pgnum = Zathura.PDF.page_number+pgdelta;
  for (int f = 0; f < 2; ++f) {
    if (pgnum >= 0 && pgnum < Zathura.PDF.number_of_pages) {
      (void)create_page_surface(pgnum);
      pgnum += pgdelta;
    } else {
      break;
    }
  }

  // just in case
  if (Zathura.UI.scroll_timer_id <= 0) {
    convert_screen_document_offset_to_pos(currypos, &Zathura.PDF.page_number, &Zathura.PDF.page_yskip_t, &Zathura.PDF.page_yskip_pix);
    render_view(Zathura.UI.drawing_area, FALSE); // no full clear
    update_status();
    return;
  }
}


//==========================================================================
//
//  sc_scroll
//
//==========================================================================
void sc_scroll (Argument *argument) {
  GtkAdjustment *adjustment;

  if (argument->n == LEFT || argument->n == RIGHT) {
    adjustment = gtk_scrolled_window_get_hadjustment(Zathura.UI.view);
  } else {
    adjustment = gtk_scrolled_window_get_vadjustment(Zathura.UI.view);
  }

  if (argument->n == WINDOW_UP || argument->n == WINDOW_DOWN ||
      argument->n == UP || argument->n == DOWN || argument->n == FULL_WINDOW_DOWN)
  {
    adjustment = gtk_scrolled_window_get_vadjustment(Zathura.UI.view);
    int window_y = gtk_adjustment_get_page_size(adjustment);
    int pgdelta = (get_page_scroll_mode() ? 0 : window_y/6);
    if (pgdelta < 0) pgdelta = 0; // are you nuts?
    int delta =
      argument->n == WINDOW_UP || argument->n == WINDOW_DOWN ? window_y-pgdelta :
      argument->n == FULL_WINDOW_DOWN ? window_y :
      64;
    // use current smooth scrolling destination if we can
    // hack for full-page scroll, why not?
    // if we're at the page start, skip the previous gap
    if (argument->n == WINDOW_UP || argument->n == UP) {
      // scrolling up
      if (Zathura.PDF.page_yskip_t == 0.0f && Zathura.PDF.page_number-1 > 0) {
        Zathura.PDF.page_number -= 1;
        Zathura.PDF.page_yskip_t = 1.0f;
        Zathura.PDF.page_yskip_pix = 0;
      }
    } else {
      // scrolling down
      if (Zathura.PDF.page_yskip_t >= 1.0f && Zathura.PDF.page_number+1 < Zathura.PDF.number_of_pages) {
        Zathura.PDF.page_number += 1;
        Zathura.PDF.page_yskip_t = 0;
        Zathura.PDF.page_yskip_pix = 0;
      }
    }
    int savedypos = calc_current_document_screen_offset();
    int prevypos = get_current_smooth_destination();
    if (prevypos < 0) {
      prevypos = savedypos;
    } else {
      // temporarily jump to the new destination
      convert_screen_document_offset_to_pos(prevypos, &Zathura.PDF.page_number, &Zathura.PDF.page_yskip_t, &Zathura.PDF.page_yskip_pix);
    }
    if (argument->n == WINDOW_UP || argument->n == UP) {
      scroll_up_pixels_no_draw(delta);
    } else {
      scroll_down_pixels_no_draw(delta);
    }
    // hack for full-page scroll, why not?
    // if we're moved past the page into the gap, skip the gap too
    if (Zathura.PDF.page_yskip_t >= 1.0f && Zathura.PDF.page_number+1 < Zathura.PDF.number_of_pages) {
      Zathura.PDF.page_number += 1;
      Zathura.PDF.page_yskip_t = 0;
      Zathura.PDF.page_yskip_pix = 0;
    }
    // scroll?
    if (smooth_scrolling > 0) {
      const int currypos = calc_current_document_screen_offset();
      // restore old destination
      convert_screen_document_offset_to_pos(savedypos, &Zathura.PDF.page_number, &Zathura.PDF.page_yskip_t, &Zathura.PDF.page_yskip_pix);
      setup_smooth_scroll_to(currypos, (int)smooth_scrolling);
    } else {
      int top_page = Zathura.PDF.page_number;
      // setup new page
      if (top_page >= 0 && top_page < Zathura.PDF.number_of_pages) {
        draw(top_page);
        /*gtk_adjustment_set_value(adjustment, 0);*/
        update_status();
      }
      //gtk_adjustment_set_value(adjustment, 0);
    }
    update_status();
    return;
  }

  /* just in case */
  gdouble value = gtk_adjustment_get_value(adjustment);
  if (value != 0) {
    value = 0;
    gtk_adjustment_set_value(adjustment, 0);
  }

  if (argument->n == UP || argument->n == FULL_UP) {
    int old_page = Zathura.PDF.page_number;
    Argument arg;
    arg.n = PREVIOUS;
    sc_navigate(&arg);
    if (scroll_wrap || Zathura.PDF.page_number < old_page) {
      arg.n = BOTTOM;
      sc_scroll(&arg);
    }
    return;
  } else if (argument->n == DOWN || argument->n == FULL_DOWN) {
    Argument arg;
    arg.n = NEXT;
    sc_navigate(&arg);
    return;
  }

  update_status();
}


//==========================================================================
//
//  sc_search
//
//==========================================================================
void sc_search (Argument *argument) {
  if (!Zathura.PDF.document || Zathura.PDF.number_of_pages < 1) return;

  //fprintf(stderr, "sc_search: %d <%s>\n", argument->n, (const char *)argument->data);

  const gchar *search_text = (const gchar *)argument->data;

  /* do not search for only spaces */
  if (search_text && search_text[0]) {
    int only_spaces = 1;
    for (const gchar *ss = search_text; *ss; ++ss) {
      if ((unsigned)(ss[0]&0xff) > 32) {
        only_spaces = 0;
        break;
      }
    }
    if (only_spaces) {
      /* hide previous search */
      if (Zathura.Search.active) {
        Zathura.Search.active = FALSE;
        surfCacheEvictHighlighted();
        draw(-1);
      }
      return;
    }
  }

  /* this is called when "return" pressed; repeat search if input string is empty */
  if (argument->n == NO_SEARCH_UP || argument->n == NO_SEARCH_DOWN) {
    if (search_text && search_text[0]) return; /* just "return" */
    argument->n = (argument->n == NO_SEARCH_UP ? BACKWARD : FORWARD);
    search_text = NULL;
  }

  int new_search = 1;
  int direction = (argument->n == BACKWARD || argument->n == UP ? -1 : 1);
  int stpage = Zathura.PDF.page_number;

  /* have old query? */
  if (Zathura.Search.query) {
    /* if it is the same query, search from the next page */
    if (!search_text || !search_text[0] || strcmp(Zathura.Search.query, search_text) == 0) {
      stpage = (stpage+Zathura.PDF.number_of_pages+direction)%Zathura.PDF.number_of_pages;
      new_search = (Zathura.Search.active ? 0 : 1);
    } else {
      /* save new search */
      g_free(Zathura.Search.query);
      Zathura.Search.query = g_strdup(argument->data);
    }
  } else {
    /* no old search, no query? */
    if (!search_text || !search_text[0]) {
      /* hide search */
      if (Zathura.Search.active) {
        Zathura.Search.active = FALSE;
        surfCacheEvictHighlighted();
        draw(-1);
      }
      return;
    }
    // set new search query
    if (Zathura.Search.query) g_free(Zathura.Search.query);
    Zathura.Search.query = g_strdup(search_text);
  }

  /* evict all pages with results for new search */
  if (new_search) surfCacheEvictHighlighted();

  Zathura.Search.active = TRUE;

  /* go back one page */
  stpage = (stpage+Zathura.PDF.number_of_pages-direction)%Zathura.PDF.number_of_pages;
  /* search */
  for (int count = Zathura.PDF.number_of_pages; count > 0; --count) {
    /* next page */
    stpage = (stpage+Zathura.PDF.number_of_pages+direction)%Zathura.PDF.number_of_pages;
    /* if this is old search, check page cache first */
    if (!new_search) {
      PageSurfCache *cc = surfCacheFind(stpage);
      /* if we have old page with ready results, use it */
      if (cc) {
        if (cc->search_results) {
          //fprintf(stderr, "OLD PAGE #%d for <%s>\n", stpage, Zathura.Search.query);
          set_page(stpage);
          update_status();
          return;
        }
        /* this page has no results, continue searching */
        continue;
      }
    }
    /* either new search, or no cached page */
    Page *current_page = Zathura.PDF.pages[stpage];
    GList *results = poppler_page_find_text(current_page->page, Zathura.Search.query);
    if (results) {
      //fprintf(stderr, "NEW PAGE #%d for <%s>\n", stpage, Zathura.Search.query);
      /* evict page, so it will be rehighlighted */
      /* TODO: don't repeat search here, pass result to page surface creator */
      surfCacheEvictPage(stpage);
      g_list_free(results);
      set_page(stpage);
      update_status();
      return;
    }
  }

  /* didn't found anything */
  Zathura.Search.active = FALSE;
  surfCacheEvictHighlighted();
  draw(-1);
  //fprintf(stderr, "NOTHING for <%s>\n", Zathura.Search.query);
}


//==========================================================================
//
//  sc_switch_goto_mode
//
//==========================================================================
void sc_switch_goto_mode (Argument *argument) {
  switch (Zathura.Global.goto_mode) {
    case GOTO_LABELS: Zathura.Global.goto_mode = GOTO_OFFSET; break;
    case GOTO_OFFSET: Zathura.Global.goto_mode = GOTO_DEFAULT; break;
    default:
      if (Zathura.Global.enable_labelmode) Zathura.Global.goto_mode = GOTO_LABELS; else Zathura.Global.goto_mode = GOTO_OFFSET;
      break;
  }
  update_status();
}


//==========================================================================
//
//  cb_index_row_activated
//
//==========================================================================
gboolean cb_index_row_activated (GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data) {
  GtkTreeModel *model;
  GtkTreeIter iter;

  g_object_get(treeview, "model", &model, NULL);

  if (gtk_tree_model_get_iter(model, &iter, path)) {
    PopplerAction *action;
    PopplerDest *destination;

    gtk_tree_model_get(model, &iter, 1, &action, -1);
    if (!action) return TRUE;

    if (action->type == POPPLER_ACTION_GOTO_DEST) {
      destination = action->goto_dest.dest;
      int page_number = destination->page_num;
      if (action->goto_dest.dest->type == POPPLER_DEST_NAMED) {
        PopplerDest *d = poppler_document_find_dest(Zathura.PDF.document, action->goto_dest.dest->named_dest);
        if (d) {
          page_number = d->page_num;
          poppler_dest_free(d);
        }
      }
      set_page(page_number-1);
      update_status();
      Zathura.Global.show_index = FALSE;
      gtk_widget_grab_focus(GTK_WIDGET(Zathura.UI.document));
    }
  }

  Zathura.Global.mode = NORMAL;
  g_object_unref(model);

  return TRUE;
}


//==========================================================================
//
//  sc_navigate_index
//
//==========================================================================
void sc_navigate_index (Argument *argument) {
  if (!Zathura.UI.index) return;

  GtkTreeView *treeview = gtk_container_get_children(GTK_CONTAINER(Zathura.UI.index))->data;
  GtkTreePath *path;

  gtk_tree_view_get_cursor(treeview, &path, NULL);
  if (!path) return;

  GtkTreeModel *model = gtk_tree_view_get_model(treeview);
  GtkTreeIter iter;
  GtkTreeIter child_iter;

  gboolean is_valid_path = TRUE;

  switch (argument->n) {
    case UP:
      if (!gtk_tree_path_prev(path)) {
        is_valid_path = (gtk_tree_path_get_depth(path) > 1 && gtk_tree_path_up(path));
      } else {
        /* row above */
        while (gtk_tree_view_row_expanded(treeview, path)) {
          gtk_tree_model_get_iter(model, &iter, path);
          /* select last child */
          gtk_tree_model_iter_nth_child(model, &child_iter, &iter, gtk_tree_model_iter_n_children(model, &iter)-1);
          gtk_tree_path_free(path);
          path = gtk_tree_model_get_path(model, &child_iter);
        }
      }
      break;
    case COLLAPSE:
      if (!gtk_tree_view_collapse_row(treeview, path) && gtk_tree_path_get_depth(path) > 1) {
        gtk_tree_path_up(path);
        gtk_tree_view_collapse_row(treeview, path);
      }
      break;
    case DOWN:
      if (gtk_tree_view_row_expanded(treeview, path)) {
        gtk_tree_path_down(path);
      } else {
        do {
          gtk_tree_model_get_iter(model, &iter, path);
          if (gtk_tree_model_iter_next(model, &iter)) {
            path = gtk_tree_model_get_path(model, &iter);
            break;
          }
        } while ((is_valid_path = (gtk_tree_path_get_depth(path) > 1)) && gtk_tree_path_up(path));
      }
      break;
    case EXPAND:
      if (gtk_tree_view_expand_row(treeview, path, FALSE)) gtk_tree_path_down(path);
      break;
    case SELECT:
      cb_index_row_activated(treeview, path, NULL, NULL);
      return;
  }

  if (is_valid_path) gtk_tree_view_set_cursor(treeview, path, NULL, FALSE);

  gtk_tree_path_free(path);
}


//==========================================================================
//
//  sc_toggle_index
//
//==========================================================================
void sc_toggle_index (Argument *argument) {
  if (!Zathura.PDF.document) return;

  GtkWidget *treeview;
  GtkTreeModel *model;
  GtkCellRenderer *renderer;
  PopplerIndexIter *iter;

  if (!Zathura.UI.index) {
    Zathura.UI.index = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(Zathura.UI.index), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    if ((iter = poppler_index_iter_new(Zathura.PDF.document))) {
      model = GTK_TREE_MODEL(gtk_tree_store_new(2, G_TYPE_STRING, G_TYPE_POINTER));
      build_index(model, NULL, iter);
      poppler_index_iter_free(iter);
    } else {
      notify(WARNING, "This document does not contain any index");
      Zathura.UI.index = NULL;
      return;
    }

    treeview = gtk_tree_view_new_with_model (model);
    g_object_unref(model);
    renderer = gtk_cell_renderer_text_new();
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW (treeview), 0, "Title", renderer, "markup", 0, NULL);
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(treeview), FALSE);
    g_object_set(G_OBJECT(renderer), "ellipsize", PANGO_ELLIPSIZE_END, NULL);
    g_object_set(G_OBJECT(gtk_tree_view_get_column(GTK_TREE_VIEW(treeview), 0)), "expand", TRUE, NULL);

    gtk_tree_view_set_cursor(GTK_TREE_VIEW(treeview), gtk_tree_path_new_first(), NULL, FALSE);
    g_signal_connect(G_OBJECT(treeview), "row-activated", G_CALLBACK(cb_index_row_activated), NULL);

    gtk_container_add (GTK_CONTAINER (Zathura.UI.index), treeview);
    gtk_widget_show (treeview);
    gtk_widget_show(Zathura.UI.index);
  }

  if (!Zathura.Global.show_index) {
    switch_view(Zathura.UI.index);
    Zathura.Global.mode = INDEX;
  } else {
    switch_view(Zathura.UI.document);
    Zathura.Global.mode = NORMAL;
  }

  Zathura.Global.show_index = !Zathura.Global.show_index;
}


//==========================================================================
//
//  sc_toggle_inputbar
//
//==========================================================================
void sc_toggle_inputbar (Argument *argument) {
  if (GTK_WIDGET_VISIBLE(GTK_WIDGET(Zathura.UI.inputbar))) {
    gtk_widget_hide(GTK_WIDGET(Zathura.UI.inputbar));
  } else {
    gtk_widget_show(GTK_WIDGET(Zathura.UI.inputbar));
  }
}


//==========================================================================
//
//  sc_toggle_fullscreen
//
//==========================================================================
void sc_toggle_fullscreen (Argument *argument) {
  static gboolean fs = TRUE;

  if (fs) {
    gtk_window_fullscreen(GTK_WINDOW(Zathura.UI.window));
    gtk_widget_hide(GTK_WIDGET(Zathura.UI.inputbar));
    gtk_widget_hide(GTK_WIDGET(Zathura.UI.statusbar));

    Argument arg;
    arg.n = ADJUST_BESTFIT;
    sc_adjust_window(&arg);

    Zathura.Global.mode = FULLSCREEN;
    fs = FALSE;
  } else {
    gtk_window_unfullscreen(GTK_WINDOW(Zathura.UI.window));
    gtk_widget_show(GTK_WIDGET(Zathura.UI.inputbar));
    gtk_widget_show(GTK_WIDGET(Zathura.UI.statusbar));

    Zathura.Global.mode = NORMAL;
    fs = TRUE;
  }
  isc_abort(NULL);
}


//==========================================================================
//
//  sc_toggle_statusbar
//
//==========================================================================
void sc_toggle_statusbar (Argument *argument) {
  if (GTK_WIDGET_VISIBLE(GTK_WIDGET(Zathura.UI.statusbar))) {
    gtk_widget_hide(GTK_WIDGET(Zathura.UI.statusbar));
  } else {
    gtk_widget_show(GTK_WIDGET(Zathura.UI.statusbar));
  }
}


//==========================================================================
//
//  sc_quit
//
//==========================================================================
void sc_quit (Argument *argument) {
  cb_destroy(NULL, NULL);
}


//==========================================================================
//
//  sc_zoom
//
//==========================================================================
void sc_zoom (Argument *argument) {
  bcmd_zoom(NULL, argument);
}


//**************************************************************************
//
// inputbar shortcut declarations
//
//**************************************************************************

//==========================================================================
//
//  isc_abort
//
//==========================================================================
void isc_abort (Argument *argument) {
  Argument arg = { HIDE };
  isc_completion(&arg);

  notify(DEFAULT, "");
  change_mode(NORMAL);
  gtk_widget_grab_focus(GTK_WIDGET(Zathura.UI.view));

  if (!Zathura.Global.show_inputbar) gtk_widget_hide(GTK_WIDGET(Zathura.UI.inputbar));

  /* replace default inputbar handler */
  g_signal_handler_disconnect((gpointer) Zathura.UI.inputbar, Zathura.Handler.inputbar_activate);
  Zathura.Handler.inputbar_activate = g_signal_connect(G_OBJECT(Zathura.UI.inputbar), "activate", G_CALLBACK(cb_inputbar_activate), NULL);
  sc_abort(NULL);
}


//==========================================================================
//
//  isc_command_history
//
//==========================================================================
void isc_command_history (Argument *argument) {
  static int current = 0;
  int length = g_list_length(Zathura.Global.history);
  if (length > 0) {
    current = (length+current+(argument->n == NEXT ? 1 : -1))%length;
    gchar *command = (gchar *)g_list_nth_data(Zathura.Global.history, current);
    notify(DEFAULT, command);
    gtk_widget_grab_focus(GTK_WIDGET(Zathura.UI.inputbar));
    gtk_editable_set_position(GTK_EDITABLE(Zathura.UI.inputbar), -1);
  }
}


//==========================================================================
//
//  isc_completion
//
//==========================================================================
void isc_completion (Argument *argument) {
  gchar *input = gtk_editable_get_chars(GTK_EDITABLE(Zathura.UI.inputbar), 1, -1);
  gchar *tmp_string = gtk_editable_get_chars(GTK_EDITABLE(Zathura.UI.inputbar), 0,  1);
  gchar identifier = tmp_string[0];
  int length = strlen(input);

  if (!input || !tmp_string) {
    if (input) g_free(input);
    if (tmp_string) g_free(tmp_string);
    return;
  }

  /* get current information*/
  char *first_space = strstr(input, " ");
  char *current_command;
  char *current_parameter;
  int current_command_length;

  if (!first_space) {
    current_command = g_strdup(input);
    current_command_length = length;
    current_parameter = NULL;
  } else {
    int offset = abs(input-first_space);
    current_command = g_strndup(input, offset);
    current_command_length = strlen(current_command);
    current_parameter = input+offset+1;
  }

  /* if the identifier does not match the command sign and
   * the completion should not be hidden, leave this function */
  if (identifier != ':' && argument->n != HIDE) {
    if (current_command) g_free(current_command);
    if (input) g_free(input);
    if (tmp_string) g_free(tmp_string);
    return;
  }

  /* static elements */
  static GtkBox *results = NULL;
  static CompletionRow *rows = NULL;

  static int current_item = 0;
  static int n_items = 0;

  static char *previous_command = NULL;
  static char *previous_parameter = NULL;
  static int previous_id = 0;
  static int previous_length = 0;

  static gboolean command_mode = TRUE;

  /* delete old list iff
   *   the completion should be hidden
   *   the current command differs from the previous one
   *   the current parameter differs from the previous one
   */
  if (argument->n == HIDE || previous_length != length ||
      (current_parameter && previous_parameter && strcmp(current_parameter, previous_parameter) != 0) ||
      (current_command && previous_command && strcmp(current_command, previous_command) != 0)) {
    if (results) gtk_widget_destroy(GTK_WIDGET(results));
    results = NULL;
    if (rows) {
      for (int i = 0; i != n_items; ++i) {
        g_free(rows[i].command);
        g_free(rows[i].description);
      }
      free(rows);
    }
    rows = NULL;
    current_item = 0;
    n_items = 0;
    command_mode = TRUE;
    if (argument->n == HIDE) {
      if (current_command) g_free(current_command);
      if (input) g_free(input);
      if (tmp_string) g_free(tmp_string);
      return;
     }
  }

  /* create new list iff
   *  there is no current list
   *  the current command differs from the previous one
   *  the current parameter differs from the previous one
   */
  if (!results) {
    results = GTK_BOX(gtk_vbox_new(FALSE, 0));
    /* create list based on parameters iff
     *  there is a current parameter given
     *  there is an old list with commands
     *  the current command does not differ from the previous one
     *  the current command has an completion function
     */
    if (strchr(input, ' ')) {
      gboolean search_matching_command = FALSE;
      for (int i = 0; i < LENGTH(commands); ++i) {
        int abbr_length = (commands[i].abbr ? strlen(commands[i].abbr) : 0);
        int cmd_length = (commands[i].command ? strlen(commands[i].command) : 0);
        if ((current_command_length <= cmd_length && !strncmp(current_command, commands[i].command, current_command_length)) ||
            (current_command_length <= abbr_length && !strncmp(current_command, commands[i].abbr, current_command_length))) {
          if (commands[i].completion) {
            previous_command = current_command;
            previous_id = i;
            search_matching_command = TRUE;
          } else {
            if (current_command) g_free(current_command);
            if (input) g_free(input);
            if (tmp_string) g_free(tmp_string);
            return;
          }
        }
      }

      if (!search_matching_command) {
        if (current_command) g_free(current_command);
        if (input) g_free(input);
        if (tmp_string) g_free(tmp_string);
        return;
      }

      Completion *result = commands[previous_id].completion(current_parameter);

      if (!result || !result->groups) {
        if (current_command) g_free(current_command);
        if (input) g_free(input);
        if (tmp_string) g_free(tmp_string);
        return;
      }

      command_mode = FALSE;
      CompletionGroup *group = NULL;
      CompletionElement *element = NULL;

      rows = malloc(sizeof(CompletionRow));
      if (!rows) out_of_memory();

      for (group = result->groups; group != NULL; group = group->next) {
        int group_elements = 0;
        for (element = group->elements; element != NULL; element = element->next) {
          if (element->value) {
            if (group->value && !group_elements) {
              rows = safe_realloc((void**)&rows, n_items+1, sizeof(CompletionRow));
              if (!rows) out_of_memory();
              rows[n_items].command = g_strdup(group->value);
              rows[n_items].description = NULL;
              rows[n_items].command_id = -1;
              rows[n_items].is_group = TRUE;
              rows[n_items++].row = GTK_WIDGET(create_completion_row(results, group->value, NULL, TRUE));
            }

            rows = safe_realloc((void**)&rows, n_items+1, sizeof(CompletionRow));
            if (!rows) out_of_memory();
            rows[n_items].command = g_strdup(element->value);
            rows[n_items].description = element->description ? g_strdup(element->description) : NULL;
            rows[n_items].command_id = previous_id;
            rows[n_items].is_group = FALSE;
            rows[n_items++].row = GTK_WIDGET(create_completion_row(results, element->value, element->description, FALSE));
            ++group_elements;
          }
        }
      }

      /* clean up */
      completion_free(result);
    } else {
      /* create list based on commands */
      int i = 0;
      command_mode = TRUE;

      rows = malloc(LENGTH(commands)*sizeof(CompletionRow));
      if (!rows) out_of_memory();

      //printf("clen=%u\n", LENGTH(commands));
      for (i = 0; i < LENGTH(commands); ++i) {
        int abbr_length = commands[i].abbr ? strlen(commands[i].abbr) : 0;
        int cmd_length  = commands[i].command ? strlen(commands[i].command) : 0;

        /* add command to list iff
         *  the current command would match the command
         *  the current command would match the abbreviation
         */
        if (((current_command_length <= cmd_length) && !strncmp(current_command, commands[i].command, current_command_length)) ||
            ((current_command_length <= abbr_length) && !strncmp(current_command, commands[i].abbr, current_command_length)))
        {
          rows[n_items].command = g_strdup(commands[i].command);
          rows[n_items].description = g_strdup(commands[i].description);
          rows[n_items].command_id = i;
          rows[n_items].is_group = FALSE;
          rows[n_items++].row = GTK_WIDGET(create_completion_row(results, commands[i].command, commands[i].description, FALSE));
        }
      }

      //printf("n_items=%u\n", n_items);
      rows = safe_realloc((void**)&rows, (n_items+1), sizeof(CompletionRow));
      if (!rows) out_of_memory();
    }

    gtk_box_pack_start(Zathura.UI.box, GTK_WIDGET(results), FALSE, FALSE, 0);
    gtk_widget_show_all(GTK_WIDGET(Zathura.UI.window));

    current_item = (argument->n == NEXT ? -1 : 0);
  }

  /* update coloring iff
   *  there is a list with items
   */
  if (results && n_items > 0) {
    if (argument->n == LAST) current_item = n_items-1;

    set_completion_row_color(results, NORMAL, current_item);
    char* temp;
    int i = 0, next_group = 0;

    for (i = 0; i < n_items; ++i) {
      if (argument->n == NEXT || argument->n == NEXT_GROUP) {
        current_item = (current_item+n_items+1)%n_items;
      } else if (argument->n == PREVIOUS || argument->n == PREVIOUS_GROUP) {
        current_item = (current_item+n_items-1)%n_items;
      }

      if (rows[current_item].is_group) {
        if (!command_mode && (argument->n == NEXT_GROUP || argument->n == PREVIOUS_GROUP)) next_group = 1;
        continue;
      } else {
        if (!command_mode && (next_group == 0) && (argument->n == NEXT_GROUP || argument->n == PREVIOUS_GROUP)) continue;
        break;
      }
    }

    set_completion_row_color(results, HIGHLIGHT, current_item);

    /* hide other items */
    int uh = ceil(n_completion_items/2);
    int lh = floor(n_completion_items/2);

    for (i = 0; i < n_items; ++i) {
     if (n_items > 1 &&
         ((i >= (current_item-lh) && (i <= current_item+uh)) ||
          (i < n_completion_items && current_item < lh) ||
          (i >= (n_items-n_completion_items) && (current_item >= (n_items-uh)))))
      {
        gtk_widget_show(rows[i].row);
      } else {
        gtk_widget_hide(rows[i].row);
      }
    }

    if (command_mode) {
      temp = g_strconcat(":", rows[current_item].command, (n_items == 1 ? " " : NULL), NULL);
    } else {
      temp = g_strconcat(":", previous_command, " ", rows[current_item].command, NULL);
    }

    gtk_entry_set_text(Zathura.UI.inputbar, temp);
    gtk_editable_set_position(GTK_EDITABLE(Zathura.UI.inputbar), -1);
    g_free(temp);

    previous_command = g_strdup((command_mode) ? rows[current_item].command : current_command);
    previous_parameter = g_strdup((command_mode) ? current_parameter : rows[current_item].command);
    previous_length = strlen(previous_command)+((command_mode) ? (length-current_command_length) : (strlen(previous_parameter)+1));
    previous_id = rows[current_item].command_id;
  }

  if (current_command) g_free(current_command);
  if (input) g_free(input);
  if (tmp_string) g_free(tmp_string);
}


//==========================================================================
//
//  isc_string_manipulation
//
//==========================================================================
void isc_string_manipulation (Argument *argument) {
  gchar *input = gtk_editable_get_chars(GTK_EDITABLE(Zathura.UI.inputbar), 0, -1);
  int length = strlen(input);
  int pos = gtk_editable_get_position(GTK_EDITABLE(Zathura.UI.inputbar));
  int i;

  switch (argument->n) {
    case DELETE_LAST_WORD:
      if (!pos) return;

      /* remove trailing spaces */
      i = pos-1;
      for (; i >= 0 && input[i] == ' '; --i) {}

      /* find the beginning of the word */
      while (i > 0 && input[i] != ' ' && input[i] != '/') --i;

      gtk_editable_delete_text(GTK_EDITABLE(Zathura.UI.inputbar),  i, pos);
      gtk_editable_set_position(GTK_EDITABLE(Zathura.UI.inputbar), i);
      break;
    case DELETE_LAST_CHAR:
      if (length-1 <= 0) isc_abort(NULL);
      gtk_editable_delete_text(GTK_EDITABLE(Zathura.UI.inputbar), pos-1, pos);
      break;
    case DELETE_TO_LINE_START:
      gtk_editable_delete_text(GTK_EDITABLE(Zathura.UI.inputbar), 1, pos);
      break;
    case NEXT_CHAR:
      gtk_editable_set_position(GTK_EDITABLE(Zathura.UI.inputbar), pos+1);
      break;
    case PREVIOUS_CHAR:
      gtk_editable_set_position(GTK_EDITABLE(Zathura.UI.inputbar), (pos == 0) ? 0 : pos-1);
      break;
    default: /* unreachable */
      break;
  }
}


//**************************************************************************
//
// command implementation
//
//**************************************************************************

//==========================================================================
//
//  cmd_bookmark
//
//==========================================================================
gboolean cmd_bookmark (int argc, char **argv) {
  if (!Zathura.PDF.document || argc < 1) return TRUE;

  /* get id */
  GString *id = g_string_new("");

  for (int i = 0; i < argc; ++i) {
    if (i != 0) id = g_string_append_c(id, ' ');
    id = g_string_append(id, argv[i]);
  }

  if (strlen(id->str) == 0) {
    notify(WARNING, "Can't set bookmark: bookmark name is empty");
    g_string_free(id, TRUE);
    return FALSE;
  }

  if (is_reserved_bm_name(id->str)) {
    notify(WARNING, "Can't set bookmark: reserved bookmark name");
    g_string_free(id, TRUE);
    return FALSE;
  }

  /* reload the bookmark file */
  read_bookmarks_file();

  /* check for existing bookmark to overwrite */
  for (int i = 0; i < Zathura.Bookmarks.number_of_bookmarks; ++i) {
    if (!strcmp(id->str, Zathura.Bookmarks.bookmarks[i].id)) {
      Zathura.Bookmarks.bookmarks[i].page = Zathura.PDF.page_number;
      g_string_free(id, TRUE);
      return TRUE;
    }
  }

  /* add new bookmark */
  Zathura.Bookmarks.bookmarks = safe_realloc((void**)&Zathura.Bookmarks.bookmarks, Zathura.Bookmarks.number_of_bookmarks+1, sizeof(Bookmark));
  if (!Zathura.Bookmarks.bookmarks) out_of_memory();

  Zathura.Bookmarks.bookmarks[Zathura.Bookmarks.number_of_bookmarks].id = g_strdup(id->str);
  Zathura.Bookmarks.bookmarks[Zathura.Bookmarks.number_of_bookmarks].page = Zathura.PDF.page_number;
  ++Zathura.Bookmarks.number_of_bookmarks;

  /* write the bookmark file */
  write_bookmarks_file();

  g_string_free(id, TRUE);
  return TRUE;
}


//==========================================================================
//
//  cmd_open_bookmark
//
//==========================================================================
gboolean cmd_open_bookmark (int argc, char **argv) {
  if (!Zathura.PDF.document || argc < 1) return TRUE;

  /* get id */
  GString *id = g_string_new("");

  for (int i = 0; i < argc; ++i) {
    if (i != 0) id = g_string_append_c(id, ' ');
    id = g_string_append(id, argv[i]);
  }

  /* find bookmark */
  for (int i = 0; i < Zathura.Bookmarks.number_of_bookmarks; ++i) {
    if (!strcmp(id->str, Zathura.Bookmarks.bookmarks[i].id)) {
      set_page(Zathura.Bookmarks.bookmarks[i].page);
      g_string_free(id, TRUE);
      return TRUE;
    }
  }

  notify(WARNING, "No matching bookmark found");
  g_string_free(id, TRUE);
  return FALSE;
}


//==========================================================================
//
//  cmd_close
//
//==========================================================================
gboolean cmd_close (int argc, char **argv) {
  close_file(FALSE);
  return TRUE;
}


//==========================================================================
//
//  cmd_delete_bookmark
//
//==========================================================================
gboolean cmd_delete_bookmark (int argc, char **argv) {
  if (!Zathura.PDF.document || argc < 1) return TRUE;

  /* get id */
  GString *id = g_string_new("");

  for (int i = 0; i < argc; ++i) {
    if (i != 0) id = g_string_append_c(id, ' ');
    id = g_string_append(id, argv[i]);
  }

  /* reload bookmark file */
  read_bookmarks_file();

  /* check for bookmark to delete */
  for (int i = 0; i < Zathura.Bookmarks.number_of_bookmarks; ++i) {
    if (!strcmp(id->str, Zathura.Bookmarks.bookmarks[i].id)) {
      /* update key file */
      g_key_file_remove_key(Zathura.Bookmarks.data, Zathura.PDF.file, Zathura.Bookmarks.bookmarks[i].id, NULL);

      g_free(Zathura.Bookmarks.bookmarks[i].id);
      /* update bookmarks */
      Zathura.Bookmarks.bookmarks[i].id   = Zathura.Bookmarks.bookmarks[Zathura.Bookmarks.number_of_bookmarks-1].id;
      Zathura.Bookmarks.bookmarks[i].page = Zathura.Bookmarks.bookmarks[Zathura.Bookmarks.number_of_bookmarks-1].page;
      Zathura.Bookmarks.bookmarks = safe_realloc((void**)&Zathura.Bookmarks.bookmarks, Zathura.Bookmarks.number_of_bookmarks, sizeof(Bookmark));
      if (!Zathura.Bookmarks.bookmarks) out_of_memory();

      --Zathura.Bookmarks.number_of_bookmarks;
      g_string_free(id, TRUE);

      /* write bookmark file */
      write_bookmarks_file();

      return TRUE;
    }
  }

  g_string_free(id, TRUE);
  return TRUE;
}


//==========================================================================
//
//  cmd_export
//
//==========================================================================
gboolean cmd_export (int argc, char **argv) {
  if (argc == 0 || !Zathura.PDF.document) return TRUE;

  if (argc < 2) {
    notify(WARNING, "No export path specified");
    return FALSE;
  }

  /* export images */
  if (!strcmp(argv[0], "images")) {
    int page_number;
    for (page_number = 0; page_number < Zathura.PDF.number_of_pages; ++page_number) {
      GList *image_list = poppler_page_get_image_mapping(Zathura.PDF.pages[page_number]->page);

      if (!g_list_length(image_list)) {
        notify(WARNING, "This document does not contain any images");
        return FALSE;
      }

      for (GList *images = image_list; images; images = g_list_next(images)) {
        char *file;

        PopplerImageMapping *image_mapping = (PopplerImageMapping *)images->data;
        gint image_id = image_mapping->image_id;

        cairo_surface_t *image = poppler_page_get_image(Zathura.PDF.pages[page_number]->page, image_id);

        if (!image) continue;

        char *filename  = g_strdup_printf("%s_p%i_i%i.png", Zathura.PDF.file, page_number+1, image_id);

        if (argv[1][0] == '~') {
          gchar *home_path = get_home_dir();
          file = g_strdup_printf("%s%s%s", home_path, argv[1]+1, filename);
          g_free(home_path);
        } else {
          file = g_strdup_printf("%s%s", argv[1], filename);
        }

        cairo_surface_write_to_png(image, file);

        g_free(filename);
        g_free(file);
      }
    }
  } else if(!strcmp(argv[0], "attachments")) {
    if (!poppler_document_has_attachments(Zathura.PDF.document)) {
      notify(WARNING, "PDF file has no attachments");
      return FALSE;
    }

    GList *attachment_list = poppler_document_get_attachments(Zathura.PDF.document);

    for (GList *attachments = attachment_list; attachments; attachments = g_list_next(attachments)) {
      char *file;

      PopplerAttachment *attachment = (PopplerAttachment*) attachments->data;

      if (argv[1][0] == '~') {
        gchar *home_path = get_home_dir();
        file = g_strdup_printf("%s%s%s", home_path, argv[1]+1, attachment->name);
        g_free(home_path);
      } else {
        file = g_strdup_printf("%s%s", argv[1], attachment->name);
      }

      poppler_attachment_save(attachment, file, NULL);

      g_free(file);
    }
  }

  return TRUE;
}


//==========================================================================
//
//  cmd_info
//
//==========================================================================
gboolean cmd_info (int argc, char **argv) {
  if (!Zathura.PDF.document) return TRUE;

  static gboolean visible = FALSE;

  if (!Zathura.UI.information) {
    GtkTreeIter iter;
    GtkCellRenderer *renderer;
    GtkTreeSelection *selection;

    GtkListStore *list = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);

    /* read document information */
    gchar *title;
    gchar *author;
    gchar *subject;
    gchar *keywords;
    gchar *creator;
    gchar *producer;
    GTime creation_date;
    GTime modification_date;

    g_object_get(Zathura.PDF.document,
        "title",         &title,
        "author",        &author,
        "subject",       &subject,
        "keywords",      &keywords,
        "creator",       &creator,
        "producer",      &producer,
        "creation-date", &creation_date,
        "mod-date",      &modification_date,
        NULL);

    /* append information to list */
    gtk_list_store_append(list, &iter);
    gtk_list_store_set(list, &iter, 0,  "Author",   1, author   ? author   : "", -1);
    gtk_list_store_append(list, &iter);
    gtk_list_store_set(list, &iter, 0,  "Title",    1, title    ? title    : "", -1);
    gtk_list_store_append(list, &iter);
    gtk_list_store_set(list, &iter, 0,  "Subject",  1, subject  ? subject  : "", -1);
    gtk_list_store_append(list, &iter);
    gtk_list_store_set(list, &iter, 0,  "Keywords", 1, keywords ? keywords : "", -1);
    gtk_list_store_append(list, &iter);
    gtk_list_store_set(list, &iter, 0,  "Creator",  1, creator  ? creator  : "", -1);
    gtk_list_store_append(list, &iter);
    gtk_list_store_set(list, &iter, 0,  "Producer", 1, producer ? producer : "", -1);

    Zathura.UI.information = gtk_tree_view_new_with_model(GTK_TREE_MODEL(list));
    renderer = gtk_cell_renderer_text_new();

    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(Zathura.UI.information), -1,
      "Name", renderer, "text", 0, NULL);
    gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(Zathura.UI.information), -1,
      "Value", renderer, "text", 1, NULL);

    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(Zathura.UI.information));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_SINGLE);

    gtk_widget_show_all(Zathura.UI.information);
  }

  if (!visible) {
    switch_view(Zathura.UI.information);
  } else {
    switch_view(Zathura.UI.document);
  }

  visible = !visible;

  return FALSE;
}


//==========================================================================
//
//  cmd_map
//
//==========================================================================
gboolean cmd_map (int argc, char **argv) {
  if (argc < 2) return TRUE;

  char *ks = argv[0];

  /* search for the right shortcut function */
  int sc_id = -1;

  for (int sc_c = 0; sc_c < LENGTH(shortcut_names); ++sc_c) {
    if (!strcmp(argv[1], shortcut_names[sc_c].name)) {
      sc_id = sc_c;
      break;
    }
  }

  if (sc_id == -1) {
    notify(WARNING, "No such shortcut function exists");
    return FALSE;
  }

  /* parse modifier and key */
  int mask = 0;
  int key = 0;
  int keyl = strlen(ks);
  int mode = NORMAL;

  // single key (e.g.: g)
  if (keyl == 1) {
    key = ks[0];
  } else if (keyl >= 3 && ks[0] == '<' && ks[keyl-1] == '>') {
    // modifier and key (e.g.: <S-g>)
    // special key or modifier and key/special key (e.g.: <S-g>, <Space>)
    char *specialkey = NULL;

    /* check for modifier */
    if (keyl >= 5 && ks[2] == '-') {
      /* evaluate modifier */
      switch (ks[1]) {
        case 'S': mask = GDK_SHIFT_MASK; break;
        case 'C': mask = GDK_CONTROL_MASK; break;
        case 'M': mask = GDK_MOD1_MASK; break;
      }

      /* no valid modifier */
      if (!mask) {
        notify(WARNING, "No valid modifier given.");
        return FALSE;
      }

      /* modifier and special key */
      if (keyl > 5) {
        specialkey = g_strndup(ks+3, keyl-4);
      } else {
        key = ks[3];
      }
    } else {
      specialkey = ks;
    }

    /* search special key */
    for (int g_c = 0; specialkey && g_c < LENGTH(gdk_keys); ++g_c) {
      if (!strcmp(specialkey, gdk_keys[g_c].identifier)) {
        key = gdk_keys[g_c].key;
        break;
      }
    }

    if (specialkey) g_free(specialkey);
  }

  if (!key) {
    notify(WARNING, "No valid key binding given.");
    return FALSE;
  }

  /* parse argument */
  Argument arg = {0, 0};

  if (argc >= 3) {
    int arg_id = -1;

    /* compare argument with given argument names... */
    for (int arg_c = 0; arg_c < LENGTH(argument_names); ++arg_c) {
      if (!strcmp(argv[2], argument_names[arg_c].name)) {
        arg_id = argument_names[arg_c].argument;
        break;
      }
    }

    /* if not, save it do .data */
    if (arg_id == -1) {
      arg.data = argv[2];
    } else {
      arg.n = arg_id;
    }
  }

  /* parse mode */
  if (argc >= 4) {
    for (int mode_c = 0; mode_c < LENGTH(mode_names); ++mode_c) {
      if (!strcmp(argv[3], mode_names[mode_c].name)) {
        mode = mode_names[mode_c].mode;
        break;
      }
    }
  }

  /* search for existing binding to overwrite it */
  ShortcutList *sc = Zathura.Bindings.sclist;
  while (sc && sc->next != NULL) {
    if (sc->element.key == key && sc->element.mask == mask && sc->element.mode == mode) {
      sc->element.function = shortcut_names[sc_id].function;
      sc->element.argument = arg;
      return TRUE;
    }

    sc = sc->next;
  }

  /* create new entry */
  ShortcutList *entry = malloc(sizeof(ShortcutList));
  if (!entry) out_of_memory();

  entry->element.mask = mask;
  entry->element.key = key;
  entry->element.function = shortcut_names[sc_id].function;
  entry->element.mode = mode;
  entry->element.argument = arg;
  entry->next = NULL;

  /* append to list */
  if (!Zathura.Bindings.sclist) Zathura.Bindings.sclist = entry;

  if (sc) sc->next = entry;

  return TRUE;
}


//==========================================================================
//
//  cmd_open
//
//==========================================================================
gboolean cmd_open (int argc, char **argv) {
  if (argc == 0 || strlen(argv[0]) == 0) return TRUE;

  /* assembly the arguments back to one string */
  GString *filepath = g_string_new("");
  for (int i = 0; i < argc; ++i) {
    if (i != 0) filepath = g_string_append_c(filepath, ' ');
    filepath = g_string_append(filepath, argv[i]);
  }

  gboolean res = open_file(filepath->str, NULL);
  g_string_free(filepath, TRUE);
  return res;
}


//==========================================================================
//
//  cmd_history
//
//  open file from bookmark history
//
//==========================================================================
gboolean cmd_history (int argc, char **argv) {
  if (argc == 0 || strlen(argv[0]) == 0) return TRUE;

  GString *filepath = g_string_new("");
  for (int i = 0; i < argc; ++i) {
    if (i != 0) filepath = g_string_append_c(filepath, ' ');
    filepath = g_string_append(filepath, argv[i]);
  }

  gboolean res = open_file(filepath->str, NULL);
  g_string_free(filepath, TRUE);
  return res;
}


//==========================================================================
//
//  cmd_print
//
//==========================================================================
gboolean cmd_print (int argc, char **argv) {
  notify(WARNING, "Printing is disabled.");
  return FALSE;
  /*
  if (!Zathura.PDF.document) return TRUE;

  if (argc == 0) {
    notify(WARNING, "No printer specified");
    return FALSE;
  }

  char *printer = argv[0];
  char *sites = (argc >= 2 ? g_strdup(argv[1]) : g_strdup_printf("1-%i", Zathura.PDF.number_of_pages));
  GString *addit = g_string_new("");

  for (int i = 2; i < argc; ++i) {
    if (i != 0) addit = g_string_append_c(addit, ' ');
    addit = g_string_append(addit, argv[i]);
  }

  char *escaped_filename = g_shell_quote(Zathura.PDF.file);
  char *command = g_strdup_printf(print_command, printer, sites, addit->str, escaped_filename);
  system(command);

  g_free(sites);
  g_free(escaped_filename);
  g_free(command);
  g_string_free(addit, TRUE);

  return TRUE;
  */
}


//==========================================================================
//
//  cmd_rotate
//
//==========================================================================
gboolean cmd_rotate (int argc, char **argv) {
  return TRUE;
}


//==========================================================================
//
//  cmd_set
//
//==========================================================================
gboolean cmd_set (int argc, char **argv) {
  if (argc <= 0) return FALSE;

  for (int i = 0; i < LENGTH(settings); ++i) {
    if(!strcmp(argv[0], settings[i].name)) {
      /* check var type */
      if (settings[i].type == 'b') {
        gboolean *x = (gboolean *)(settings[i].variable);
        *x = !(*x);

        if (argv[1]) {
          if (!strcmp(argv[1], "ona") || !strcmp(argv[1], "no") || !strcmp(argv[1], "off") || !strcmp(argv[1], "false") || !strcmp(argv[1], "0")) {
            *x = FALSE;
          } else {
            *x = TRUE;
          }
        }
      } else if (settings[i].type == 'i') {
        if (argc != 2) return FALSE;

        int *x = (int *)(settings[i].variable);

        int id = -1;
        for (int arg_c = 0; arg_c < LENGTH(argument_names); ++arg_c) {
          if (!strcmp(argv[1], argument_names[arg_c].name)) {
            id = argument_names[arg_c].argument;
            break;
          }
        }

        if (id == -1) id = atoi(argv[1]);

        *x = id;
      } else if (settings[i].type == 'f') {
        if (argc != 2) return FALSE;

        float *x = (float *)(settings[i].variable);
        if (argv[1]) *x = atof(argv[1]);
      } else if (settings[i].type == 's') {
        if (argc < 2) return FALSE;

        /* assembly the arguments back to one string */
        GString *s = g_string_new("");
        for (int j = 1; j < argc; ++j) {
          if (j != 1) s = g_string_append_c(s, ' ');
          s = g_string_append(s, argv[j]);
        }

        char **x = (char **)settings[i].variable;
        *x = s->str;
      } else if (settings[i].type == 'c') {
        if (argc != 2) return FALSE;

        char *x = (char *)(settings[i].variable);
        if (argv[1]) *x = argv[1][0];
      }

      /* re-init */
      if (settings[i].reinit) init_look();

      /* render */
      if (settings[i].render) {
        if (!Zathura.PDF.document) return FALSE;
        draw(Zathura.PDF.page_number);
      }
    }
  }

  update_status();
  return TRUE;
}


//==========================================================================
//
//  cmd_quit
//
//==========================================================================
gboolean cmd_quit (int argc, char **argv) {
  cb_destroy(NULL, NULL);
  return TRUE;
}


//==========================================================================
//
//  save_file
//
//==========================================================================
gboolean save_file (int argc, char **argv, gboolean overwrite) {
  if (argc == 0 || !Zathura.PDF.document) return TRUE;

  gchar *file_path = NULL;

  if (argv[0][0] == '~') {
    gchar *home_path = get_home_dir();
    file_path = g_build_filename(home_path, argv[0]+1, NULL);
    g_free(home_path);
  } else {
    file_path = g_strdup(argv[0]);
  }

  if (!overwrite && g_file_test(file_path, G_FILE_TEST_EXISTS)) {
    char *message = g_strdup_printf("File already exists: %s. Use :write! to overwrite it.", file_path);
    notify(ERROR, message);
    g_free(message);
    g_free(file_path);
    return FALSE;
  }

  char *path = NULL;
  if (file_path[0] == '/') {
    path = g_strdup_printf("file://%s", file_path);
  } else {
    char *cur = g_get_current_dir();
    path = g_strdup_printf("file://%s/%s", cur, file_path);
    g_free(cur);
  }
  g_free(file_path);

  /* format path */
  GError *error = NULL;
  if (!poppler_document_save(Zathura.PDF.document, path, &error)) {
    g_free(path);
    char *message = g_strdup_printf("Can not write file: %s", error->message);
    notify(ERROR, message);
    g_free(message);
    g_error_free(error);
    return FALSE;
  }

  g_free(path);

  return TRUE;
}


//==========================================================================
//
//  cmd_save
//
//==========================================================================
gboolean cmd_save (int argc, char **argv) {
  return save_file(argc, argv, FALSE);
}


//==========================================================================
//
//  cmd_savef
//
//==========================================================================
gboolean cmd_savef (int argc, char **argv) {
  return save_file(argc, argv, TRUE);
}


//**************************************************************************
//
// completion command implementation
//
//**************************************************************************

//==========================================================================
//
//  cc_bookmark
//
//==========================================================================
Completion *cc_bookmark (char *input) {
  Completion *completion = completion_init();
  CompletionGroup *group = completion_group_create(NULL);

  completion_add_group(completion, group);

  const int input_length = (input ? strlen(input) : 0);

  for (int i = 0; i < Zathura.Bookmarks.number_of_bookmarks; ++i) {
    if (input_length <= strlen(Zathura.Bookmarks.bookmarks[i].id) &&
        !strncmp(input, Zathura.Bookmarks.bookmarks[i].id, input_length))
    {
      completion_group_add_element(group, Zathura.Bookmarks.bookmarks[i].id, g_strdup_printf("Page %d", Zathura.Bookmarks.bookmarks[i].page));
    }
  }

  return completion;
}


//==========================================================================
//
//  cc_export
//
//==========================================================================
Completion *cc_export (char *input) {
  Completion *completion = completion_init();
  CompletionGroup *group = completion_group_create(NULL);

  completion_add_group(completion, group);

  completion_group_add_element(group, "images",      "Export images");
  completion_group_add_element(group, "attachments", "Export attachments");

  return completion;
}


//==========================================================================
//
//  cc_open
//
//==========================================================================
Completion *cc_open (char *input) {
  Completion *completion = completion_init();
  CompletionGroup *group = completion_group_create(NULL);

  completion_add_group(completion, group);

  /* ~ */
  if (input && input[0] == '~') {
    gchar* home_path = get_home_dir();
    char *file = g_strdup_printf(":open %s/%s", home_path, input+1);
    g_free(home_path);
    gtk_entry_set_text(Zathura.UI.inputbar, file);
    gtk_editable_set_position(GTK_EDITABLE(Zathura.UI.inputbar), -1);
    g_free(file);
    completion_free(completion);
    return NULL;
  }

  /* read dir */
  char *path = g_strdup("/");
  char *file = g_strdup("");
  int file_length = 0;

  /* parse input string */
  if (input && strlen(input) > 0) {
    char *dinput = g_strdup(input);
    char *binput = g_strdup(input);
    char *path_temp = dirname(dinput);
    char *file_temp = basename(binput);
    char last_char = input[strlen(input)-1];

    if (strcmp(path_temp, "/") == 0 && strcmp(file_temp, "/") == 0) {
      g_free(file);
      file = g_strdup("");
    } else if (strcmp(path_temp, "/") == 0 && strcmp(file_temp, "/") != 0 && last_char != '/') {
      g_free(file);
      file = g_strdup(file_temp);
    } else if (strcmp(path_temp, "/") == 0 && strcmp(file_temp, "/") != 0 && last_char == '/') {
      g_free(path);
      path = g_strdup_printf("/%s/", file_temp);
    } else if (last_char == '/') {
      g_free(path);
      path = g_strdup(input);
    } else {
      g_free(path);
      g_free(file);
      path = g_strdup_printf("%s/", path_temp);
      file = g_strdup(file_temp);
    }

    g_free(dinput);
    g_free(binput);
  }

  file_length = strlen(file);

  /* open directory */
  GDir *dir = g_dir_open(path, 0, NULL);
  if (!dir) {
    g_free(path);
    g_free(file);
    completion_free(completion);
    return NULL;
  }

  /* create element list */
  char *name = NULL;

  while ((name = (char *)g_dir_read_name(dir)) != NULL) {
    char *d_name = g_filename_display_name(name);
    int d_length = strlen(d_name);

    if ((file_length <= d_length && strncmp(file, d_name, file_length) == 0) ||
        file_length == 0)
    {
      char *d = g_strdup_printf("%s%s", path, d_name);
      if (g_file_test(d, G_FILE_TEST_IS_DIR)) {
        gchar *subdir = d;
        d = g_strdup_printf("%s/", subdir);
        g_free(subdir);
      }
      completion_group_add_element(group, d, NULL);
      g_free(d);
    }
    g_free(d_name);
  }

  g_dir_close(dir);
  g_free(file);
  g_free(path);

  return completion;
}


//==========================================================================
//
//  cc_is_good_history_completion
//
//==========================================================================
static int cc_is_good_history_completion (const char *fname, const char *input) {
  if (!fname || !fname[0]) return 0;
  if (strcmp(fname, main_group_name) == 0) return 0;
  /* check file name (ignore pathes) */
  if (input && input[0]) {
    if (strchr(input, '/')) {
      /* just a substring */
      if (!strstr(fname, input)) return 0;
    } else {
      /* substring in file name */
      const char *pp = strrchr(fname, '/');
      if (!pp) pp = fname;
      if (!strstr(pp, input)) return 0;
    }
  }
  /* ignore missing files */
  return (access(fname, R_OK) == 0 ? 1 : 0);
}


//==========================================================================
//
//  cc_history
//
//==========================================================================
Completion *cc_history (char *input) {
  if (!Zathura.Bookmarks.data) return NULL; /* nothing to do */

  Completion *completion = completion_init();
  CompletionGroup *group = completion_group_create(NULL);

  completion_add_group(completion, group);

  /* get all groups */
  gchar **groups = g_key_file_get_groups(Zathura.Bookmarks.data, NULL);
  if (!groups) {
    /* oops */
    completion_free(completion);
    return NULL;
  }

  /* scan groups, append all good files */
  for (gchar **grp = groups; *grp; ++grp) {
    gchar *gname = *grp;
    /* always ignore current file, it will be added last */
    if (Zathura.PDF.file && strcmp(gname, Zathura.PDF.file) == 0) continue;
    if (cc_is_good_history_completion(gname, input)) {
      /* append it (backwards) */
      //completion_group_prepend_element(group, gname, NULL);
      completion_group_add_element(group, gname, NULL);
    }
  }
  g_strfreev(groups);

  /* append current file */
  if (Zathura.PDF.file && cc_is_good_history_completion(Zathura.PDF.file, input)) {
    completion_group_add_element(group, Zathura.PDF.file, NULL);
  }

  /* limit list */
  //completion_group_limit_last(group, (input && input[0] ? 42 : 32));

  return completion;
}


//==========================================================================
//
//  cc_print
//
//==========================================================================
Completion *cc_print (char *input) {
  Completion *completion = completion_init();
  CompletionGroup *group = completion_group_create(NULL);

  completion_add_group(completion, group);

  const int input_length = (input ? strlen(input) : 0);

  /* read printers */
  char *current_line = NULL, current_char;
  int count = 0;
  FILE *fp = popen(list_printer_command, "r");

  if (!fp) {
    completion_free(completion);
    return NULL;
  }

  while ((current_char = fgetc(fp)) != EOF) {
    if (!current_line) current_line = malloc(sizeof(char));
    if (!current_line) out_of_memory();

    current_line = safe_realloc((void **)&current_line, count+1, sizeof(char));
    if (!current_line) out_of_memory();

    if (current_char != '\n') {
      current_line[count++] = current_char;
    } else {
      current_line[count] = '\0';
      const int line_length = strlen(current_line);

      if (input_length <= line_length ||
          !strncmp(input, current_line, input_length))
      {
        completion_group_add_element(group, current_line, NULL);
      }

      free(current_line);
      current_line = NULL;
      count = 0;
    }
  }

  pclose(fp);

  return completion;
}


//==========================================================================
//
//  cc_set
//
//==========================================================================
Completion *cc_set (char *input) {
  Completion *completion = completion_init();
  CompletionGroup *group = completion_group_create(NULL);

  completion_add_group(completion, group);

  const int input_length = (input ? strlen(input) : 0);

  for (int i = 0; i < LENGTH(settings); ++i) {
    if (input_length <= strlen(settings[i].name) &&
        !strncmp(input, settings[i].name, input_length))
    {
      completion_group_add_element(group, settings[i].name, settings[i].description);
    }
  }

  return completion;
}


//**************************************************************************
//
// buffer command implementation
//
//**************************************************************************

//==========================================================================
//
//  bcmd_goto
//
//==========================================================================
void bcmd_goto (char* buffer, Argument* argument) {
  if (!Zathura.PDF.document) return;

  const int b_length = strlen(buffer);
  if (b_length < 1) return;

  if (!strcmp(buffer, "gg")) {
    set_page(0);
  } else if (!strcmp(buffer, "G")) {
    set_page(Zathura.PDF.number_of_pages-1);
  } else {
    char *id = g_strndup(buffer, b_length-1);
    int pid = atoi(id);

    if (Zathura.Global.goto_mode == GOTO_LABELS) {
      for (int i = 0; i < Zathura.PDF.number_of_pages; ++i) {
        if (!strcmp(id, Zathura.PDF.pages[i]->label)) {
          pid = Zathura.PDF.pages[i]->id;
        }
      }
    }
    set_page(pid-1);
    g_free(id);
  }

  update_status();
}


//==========================================================================
//
//  try_goto
//
//==========================================================================
gboolean try_goto (const char *buffer) {
  char *endptr = NULL;
  long page_number = strtol(buffer, &endptr, 10)-1;
  if (*endptr) return FALSE; /* conversion error */
  /* behave like vim: <= 1 => first line, >= #lines => last line */
  page_number = MAX(0, MIN(Zathura.PDF.number_of_pages-1, page_number));
  set_page(page_number);
  update_status();
  return TRUE;
}


//==========================================================================
//
//  bcmd_zoom
//
//==========================================================================
void bcmd_zoom (char *buffer, Argument *argument) {
  Zathura.Global.adjust_mode = ADJUST_NONE;

  if (argument->n == ZOOM_IN) {
    if ((Zathura.PDF.scale+zoom_step) <= zoom_max) {
      Zathura.PDF.scale += zoom_step;
    } else {
      Zathura.PDF.scale = zoom_max;
    }
  } else if (argument->n == ZOOM_OUT) {
    if ((Zathura.PDF.scale-zoom_step) >= zoom_min) {
      Zathura.PDF.scale -= zoom_step;
    } else {
      Zathura.PDF.scale = zoom_min;
    }
  } else if (argument->n == ZOOM_SPECIFIC) {
    const int b_length = strlen(buffer);
    if (b_length < 1) return;

    const int value = atoi(g_strndup(buffer, b_length-1));
         if (value <= zoom_min) Zathura.PDF.scale = zoom_min;
    else if (value >= zoom_max) Zathura.PDF.scale = zoom_max;
    else Zathura.PDF.scale = value;
  } else {
    Zathura.PDF.scale = 100;
  }

  draw(Zathura.PDF.page_number);
  update_status();
}


//==========================================================================
//
//  bcmd_pgsc_toggle
//
//  toggle document-local page scroll mode
//
//==========================================================================
void bcmd_pgsc_toggle (char *buffer, Argument *argument) {
  Zathura.PDF.invert_page_scroll_mode = !Zathura.PDF.invert_page_scroll_mode;
  notify(DEFAULT, (get_page_scroll_mode() ? "Page scroll mode: full" : "Page scroll mode: 5/6"));
}


//**************************************************************************
//
// special command implementation
//
//**************************************************************************

//==========================================================================
//
//  scmd_search
//
//==========================================================================
gboolean scmd_search (gchar *input, Argument *argument) {
  if (!input || !strlen(input)) {
    if (argument->n != NO_SEARCH_UP && argument->n != NO_SEARCH_DOWN) {
      return TRUE;
    }
  }

  argument->data = input;
  sc_search(argument);

  return TRUE;
}


//**************************************************************************
//
// callback implementation
//
//**************************************************************************

//==========================================================================
//
//  cb_destroy
//
//==========================================================================
gboolean cb_destroy (GtkWidget *widget, gpointer data) {
  pango_font_description_free(Zathura.Style.font);

  if (Zathura.PDF.document) close_file(FALSE);

  /* clean up bookmarks */
  g_free(Zathura.Bookmarks.file);
  if (Zathura.Bookmarks.data) g_key_file_free(Zathura.Bookmarks.data);

  /* inotify */
  if (Zathura.FileMonitor.monitor) g_object_unref(Zathura.FileMonitor.monitor);
  if (Zathura.FileMonitor.file) g_object_unref(Zathura.FileMonitor.file);

  g_list_free(Zathura.Global.history);

  /* clean shortcut list */
  ShortcutList *sc = Zathura.Bindings.sclist;
  while (sc) {
    ShortcutList *ne = sc->next;
    free(sc);
    sc = ne;
  }

  g_free(Zathura.State.filename);
  g_free(Zathura.State.pages);

  g_free(Zathura.Config.config_dir);
  g_free(Zathura.Config.data_dir);
  if (Zathura.StdinSupport.file) g_unlink(Zathura.StdinSupport.file);
  g_free(Zathura.StdinSupport.file);

  gtk_main_quit();

  return TRUE;
}


//==========================================================================
//
//  render_view
//
//  if we don't want a full clear, we'll only clear sides and gaps
//
//==========================================================================
void render_view (GtkWidget *widget, gboolean fullclear) {
  if (!widget || !widget->window) return;

  if (!Zathura.PDF.document) {
    gdk_window_clear(widget->window);
    return;
  }

  int page_id = Zathura.PDF.page_number;
  if (page_id < 0 || page_id > Zathura.PDF.number_of_pages) {
    gdk_window_clear(widget->window);
    return;
  }

  if (fullclear) gdk_window_clear(widget->window);

  int window_x, window_y;
  gdk_drawable_get_size(widget->window, &window_x, &window_y);

  const double scale = ((double)Zathura.PDF.scale/100.0);

  int yofs = get_current_page_screen_ytop();

  while (yofs < window_y && page_id < Zathura.PDF.number_of_pages) {
    Page *current_page = Zathura.PDF.pages[page_id];
    const int width = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->width : current_page->height)*scale;
    const int height = (Zathura.PDF.rotate == 0 || Zathura.PDF.rotate == 180 ? current_page->height : current_page->width)*scale;

    int offset_x;

    if (window_x > width) {
      switch (Zathura.Global.xcenter_mode) {
        case CENTER_LEFT_TOP: offset_x = Zathura.PDF.xleft_offset; break;
        case CENTER_RIGHT_BOTTOM: offset_x = window_x-width+Zathura.PDF.xright_offset; break;
        default: offset_x = (window_x-width)/2+Zathura.PDF.xcenter_offset; break;
      }
    } else {
      offset_x = 0;
    }

    cairo_t *cairo = gdk_cairo_create(widget->window);
    cairo_surface_t *surf = create_page_surface(page_id);
    cairo_set_source_surface(cairo, surf, offset_x, yofs);
    cairo_paint(cairo);
    cairo_destroy(cairo);

    // clear sides and gap
    if (!fullclear) {
      // left part
      if (offset_x > 0) gdk_window_clear_area(widget->window, 0, yofs, offset_x, height);
      // right part
      if (offset_x+width < window_x) gdk_window_clear_area(widget->window, offset_x+width, yofs, window_x-(offset_x+width), height);
      // gap
      if (Zathura.PDF.page_gap > 0) gdk_window_clear_area(widget->window, 0, yofs+height, window_x, Zathura.PDF.page_gap);
    }

    yofs += height+Zathura.PDF.page_gap;
    page_id += 1;
  }
}


//==========================================================================
//
//  cb_draw
//
//==========================================================================
gboolean cb_draw (GtkWidget *widget, GdkEventExpose *expose, gpointer data) {
  render_view(widget, FALSE);
  return TRUE;
}


//==========================================================================
//
//  cb_inputbar_kb_pressed
//
//==========================================================================
gboolean cb_inputbar_kb_pressed (GtkWidget *widget, GdkEventKey *event, gpointer data) {
  /* inputbar shortcuts */
  for (int i = 0; i < LENGTH(inputbar_shortcuts); ++i) {
    if (event->keyval == inputbar_shortcuts[i].key &&
        ((event->state&inputbar_shortcuts[i].mask) == inputbar_shortcuts[i].mask || inputbar_shortcuts[i].mask == 0))
    {
      inputbar_shortcuts[i].function(&(inputbar_shortcuts[i].argument));
      return TRUE;
    }
  }

  /* special commands */
  char *identifier_string = gtk_editable_get_chars(GTK_EDITABLE(Zathura.UI.inputbar), 0, 1);
  char identifier = identifier_string[0];

  for (int i = 0; i < LENGTH(special_commands); ++i) {
    if (identifier == special_commands[i].identifier && special_commands[i].always == 1) {
      gchar *input = gtk_editable_get_chars(GTK_EDITABLE(Zathura.UI.inputbar), 1, -1);
      guint new_utf_char = gdk_keyval_to_unicode(event->keyval);

      if (new_utf_char != 0 && new_utf_char >= 32) {
        gchar *newchar = g_malloc0(6*sizeof(gchar));
        if (newchar == NULL) {
          g_free(input);
          continue;
        }

        gint len = g_unichar_to_utf8(new_utf_char, newchar);
        newchar[len] = 0;
        gchar *tmp = g_strconcat(input, newchar, NULL);

        g_free(input);
        g_free(newchar);

        input = tmp;
      }

      // FIXME: hack for search
      if (special_commands[i].function == scmd_search && event->keyval == GDK_Return) {
        Argument argument = { NO_SEARCH_UP, NULL };
        argument.n = (special_commands[i].argument.n == UP ? NO_SEARCH_UP : NO_SEARCH_DOWN);
        scmd_search(input, &argument);
      } else {
        special_commands[i].function(input, &(special_commands[i].argument));
      }

      g_free(identifier_string);
      g_free(input);
      return FALSE;
    }
  }

  g_free(identifier_string);

  return FALSE;
}


//==========================================================================
//
//  cb_inputbar_activate
//
//==========================================================================
gboolean cb_inputbar_activate (GtkEntry *entry, gpointer data) {
  gchar *input = gtk_editable_get_chars(GTK_EDITABLE(entry), 1, -1);
  gchar **tokens = g_strsplit(input, " ", -1);
  g_free(input);

  gchar *command = tokens[0];
  int length = g_strv_length(tokens);
  gboolean retv = FALSE;
  gboolean succ = FALSE;

  /* no input */
  if (length < 1) {
    isc_abort(NULL);
    g_strfreev(tokens);
    return FALSE;
  }

  /* append input to the command history */
  Zathura.Global.history = g_list_append(Zathura.Global.history, g_strdup(gtk_entry_get_text(entry)));

  /* special commands */
  char identifier = gtk_editable_get_chars(GTK_EDITABLE(entry), 0, 1)[0];
  for (int i = 0; i < LENGTH(special_commands); ++i) {
    if (identifier == special_commands[i].identifier) {
      /* special commands that are evaluated every key change are not called here */
      if (special_commands[i].always == 1) {
        isc_abort(NULL);
        g_strfreev(tokens);
        return TRUE;
      }

      retv = special_commands[i].function(input, &(special_commands[i].argument));
      if (retv) isc_abort(NULL);
      gtk_widget_grab_focus(GTK_WIDGET(Zathura.UI.view));
      g_strfreev(tokens);
      return TRUE;
    }
  }

  /* search commands */
  for (int i = 0; i < LENGTH(commands); ++i) {
    if (g_strcmp0(command, commands[i].command) == 0 ||
        g_strcmp0(command, commands[i].abbr) == 0)
    {
      retv = commands[i].function(length-1, tokens+1);
      succ = TRUE;
      break;
    }
  }

  if (retv) isc_abort(NULL);

  if (!succ) {
    /* it maybe a goto command */
    if (!try_goto(command)) notify(ERROR, "Unknown command.");
  }

  Argument arg = { HIDE };
  isc_completion(&arg);
  gtk_widget_grab_focus(GTK_WIDGET(Zathura.UI.view));

  g_strfreev(tokens);
  return TRUE;
}


//==========================================================================
//
//  cb_inputbar_form_activate
//
//==========================================================================
gboolean cb_inputbar_form_activate (GtkEntry *entry, gpointer data) {
  if (!Zathura.PDF.document) return TRUE;

  Page *current_page = Zathura.PDF.pages[Zathura.PDF.page_number];
  int number_of_links = 0;
  int link_id = 1;
  int new_page_id = Zathura.PDF.page_number;

  GList *link_list = poppler_page_get_link_mapping(current_page->page);
  link_list = g_list_reverse(link_list);

  if ((number_of_links = g_list_length(link_list)) <= 0) return FALSE;

  /* parse entry */
  gchar *input = gtk_editable_get_chars(GTK_EDITABLE(entry), 1, -1);
  gchar *token = input+strlen("Follow hint: ")-1;
  if (!token) return FALSE;

  int li = atoi(token);
  if (li <= 0 || li > number_of_links) {
    set_page(Zathura.PDF.page_number);
    isc_abort(NULL);
    notify(WARNING, "Invalid hint");
    return TRUE;
  }

  /* compare entry */
  for (GList *links = link_list; links; links = g_list_next(links)) {
    PopplerLinkMapping *link_mapping = (PopplerLinkMapping *)links->data;
    PopplerAction *action = link_mapping->action;

    /* only handle URI and internal links */
    if (action->type == POPPLER_ACTION_URI) {
      if (li == link_id) open_uri(action->uri.uri);
    } else if (action->type == POPPLER_ACTION_GOTO_DEST) {
      if (li == link_id) {
        if (action->goto_dest.dest->type == POPPLER_DEST_NAMED) {
          PopplerDest *destination = poppler_document_find_dest(Zathura.PDF.document, action->goto_dest.dest->named_dest);
          if (destination) {
            new_page_id = destination->page_num-1;
            poppler_dest_free(destination);
          }
        } else {
          new_page_id = action->goto_dest.dest->page_num-1;
        }
      }
    } else {
      continue;
    }
    ++link_id;
  }

  poppler_page_free_link_mapping(link_list);

  /* reset all */
  set_page(new_page_id);
  isc_abort(NULL);

  return TRUE;
}


//==========================================================================
//
//  cb_inputbar_password_activate
//
//==========================================================================
gboolean cb_inputbar_password_activate (GtkEntry *entry, gpointer data) {
  gchar *input = gtk_editable_get_chars(GTK_EDITABLE(entry), 1, -1);
  gchar *token = input+strlen("Enter password: ")-1;
  if (!token) return FALSE;

  if (!open_file(Zathura.PDF.file, token)) {
    enter_password();
    return TRUE;
  }

  /* replace default inputbar handler */
  g_signal_handler_disconnect((gpointer) Zathura.UI.inputbar, Zathura.Handler.inputbar_activate);
  Zathura.Handler.inputbar_activate = g_signal_connect(G_OBJECT(Zathura.UI.inputbar), "activate", G_CALLBACK(cb_inputbar_activate), NULL);

  isc_abort(NULL);

  return TRUE;
}


//==========================================================================
//
//  cb_view_kb_pressed
//
//==========================================================================
gboolean cb_view_kb_pressed (GtkWidget *widget, GdkEventKey *event, gpointer data) {
  //fprintf(stderr, "KEY=0x%04X\n", event->keyval);
  ShortcutList *sc = Zathura.Bindings.sclist;
  while (sc) {
    if (event->keyval == sc->element.key &&
        (CLEAN(event->state) == sc->element.mask || (sc->element.key >= 0x21 &&
        sc->element.key <= 0x7E && CLEAN(event->state) == GDK_SHIFT_MASK)) &&
        (Zathura.Global.mode&sc->element.mode || sc->element.mode == ALL) &&
        sc->element.function)
    {
      if (!(Zathura.Global.buffer && strlen(Zathura.Global.buffer->str)) || (sc->element.mask == GDK_CONTROL_MASK) ||
          (sc->element.key <= 0x21 || sc->element.key >= 0x7E))
      {
        sc->element.function(&(sc->element.argument));
        return TRUE;
      }
    }

    sc = sc->next;
  }

  if (Zathura.Global.mode == ADD_MARKER) {
    add_marker(event->keyval);
    change_mode(NORMAL);
    return TRUE;
  } else if (Zathura.Global.mode == EVAL_MARKER) {
    eval_marker(event->keyval);
    change_mode(NORMAL);
    return TRUE;
  }

  /* append only numbers and characters to buffer */
  if (event->keyval >= 0x21 && event->keyval <= 0x7E) {
    if (!Zathura.Global.buffer) Zathura.Global.buffer = g_string_new("");
    Zathura.Global.buffer = g_string_append_c(Zathura.Global.buffer, event->keyval);
    gtk_label_set_markup((GtkLabel *)Zathura.Global.status_buffer, Zathura.Global.buffer->str);
  }

  /* search buffer commands */
  if (Zathura.Global.buffer) {
    for (int i = 0; i < LENGTH(buffer_commands); ++i) {
      regex_t regex;
      regcomp(&regex, buffer_commands[i].regex, REG_EXTENDED);
      int status = regexec(&regex, Zathura.Global.buffer->str, (size_t) 0, NULL, 0);
      regfree(&regex);
      if (status == 0) {
        buffer_commands[i].function(Zathura.Global.buffer->str, &(buffer_commands[i].argument));
        g_string_free(Zathura.Global.buffer, TRUE);
        Zathura.Global.buffer = NULL;
        gtk_label_set_markup((GtkLabel*) Zathura.Global.status_buffer, "");
        return TRUE;
      }
    }
  }

  return FALSE;
}


//==========================================================================
//
//  cb_view_resized
//
//==========================================================================
gboolean cb_view_resized (GtkWidget *widget, GtkAllocation *allocation, gpointer data) {
  Argument arg;
  arg.n = Zathura.Global.adjust_mode;
  sc_adjust_window(&arg);
  return TRUE;
}


//==========================================================================
//
//  cb_view_button_pressed
//
//==========================================================================
gboolean cb_view_button_pressed (GtkWidget *widget, GdkEventButton *event, gpointer data) {
  if (!Zathura.PDF.document) return FALSE;

  /* clean page */
  draw(Zathura.PDF.page_number);
  Zathura.SelectPoint.x = event->x;
  Zathura.SelectPoint.y = event->y;

  return TRUE;
}


//==========================================================================
//
//  cb_view_button_release
//
//==========================================================================
gboolean cb_view_button_release (GtkWidget *widget, GdkEventButton *event, gpointer data) {
  if (!Zathura.PDF.document) return FALSE;

  double offset_x = 0, offset_y = 0;
  int page_id = -1;
  PopplerRectangle rectangle;

  /* build selection rectangle */
  rectangle.x1 = event->x;
  rectangle.y1 = event->y;

  rectangle.x2 = Zathura.SelectPoint.x;
  rectangle.y2 = Zathura.SelectPoint.y;

  /* calculate offset */
  calculate_screen_offset(widget, event->x, event->y, &offset_x, &offset_y, &page_id);
  if (page_id < 0) return FALSE; // not on any page

  /* draw selection rectangle */
  /* first, evict page surface, because we need it recreated; this is a hack */
  surfCacheEvictPage(page_id);

  /* create page surface, and draw rectangle onto it */
  cairo_t *cairo = cairo_create(create_page_surface(page_id));
  cairo_set_source_rgba(cairo, Zathura.Style.select_text.red, Zathura.Style.select_text.green, Zathura.Style.select_text.blue, transparency);
  cairo_rectangle(cairo, rectangle.x1-offset_x, rectangle.y1-offset_y, (rectangle.x2-rectangle.x1), (rectangle.y2-rectangle.y1));
  cairo_fill(cairo);
  cairo_destroy(cairo);
  gtk_widget_queue_draw(Zathura.UI.drawing_area);

  /* resize selection rectangle to document page */
  Page *current_page = Zathura.PDF.pages[page_id];
  const double page_width = current_page->width;
  const double page_height = current_page->height;
  const double scale = ((double)Zathura.PDF.scale/100.0);

  rectangle.x1 = (rectangle.x1-offset_x)/scale;
  rectangle.y1 = (rectangle.y1-offset_y)/scale;
  rectangle.x2 = (rectangle.x2-offset_x)/scale;
  rectangle.y2 = (rectangle.y2-offset_y)/scale;

  /* rotation */
  int rotate = Zathura.PDF.rotate;
  double x1 = rectangle.x1;
  double x2 = rectangle.x2;
  double y1 = rectangle.y1;
  double y2 = rectangle.y2;

  switch (rotate) {
    case 90:
      rectangle.x1 = y1;
      rectangle.y1 = page_height-x2;
      rectangle.x2 = y2;
      rectangle.y2 = page_height-x1;
      break;
    case 180:
      rectangle.x1 = (page_height-y1);
      rectangle.y1 = (page_width-x2);
      rectangle.x2 = (page_height-y2);
      rectangle.y2 = (page_width-x1);
      break;
    case 270:
      rectangle.x1 = page_width-y2;
      rectangle.y1 = x1;
      rectangle.x2 = page_width-y1;
      rectangle.y2 = x2;
      break;
  }

  /* reset points of the rectangle so that p1 is in the top-left corner
   * and p2 is in the bottom right corner */
  if (rectangle.x1 > rectangle.x2) {
    const double d = rectangle.x1;
    rectangle.x1 = rectangle.x2;
    rectangle.x2 = d;
  }
  if (rectangle.y2 > rectangle.y1) {
    const double d = rectangle.y1;
    rectangle.y1 = rectangle.y2;
    rectangle.y2 = d;
  }

  #if !POPPLER_CHECK_VERSION(0,15,0)
  /* adapt y coordinates */
  rectangle.y1 = page_height-rectangle.y1;
  rectangle.y2 = page_height-rectangle.y2;
  #endif

  /* get selected text */
  char *selected_text = poppler_page_get_selected_text(Zathura.PDF.pages[page_id]->page, SELECTION_STYLE, &rectangle);
  if (selected_text) {
    gtk_clipboard_set_text(gtk_clipboard_get(GDK_SELECTION_PRIMARY), selected_text, -1);
    g_free(selected_text);
  }

  return TRUE;
}


//==========================================================================
//
//  cb_view_motion_notify
//
//==========================================================================
gboolean cb_view_motion_notify (GtkWidget *widget, GdkEventMotion *event, gpointer data) {
  return TRUE;
}


//==========================================================================
//
//  cb_view_scrolled
//
//==========================================================================
gboolean cb_view_scrolled (GtkWidget *widget, GdkEventScroll *event, gpointer data) {
  for (int i = 0; i < LENGTH(mouse_scroll_events); ++i) {
    if (event->direction == mouse_scroll_events[i].direction) {
      mouse_scroll_events[i].function(&(mouse_scroll_events[i].argument));
      return TRUE;
    }
  }
  return FALSE;
}


//==========================================================================
//
//  cb_watch_file
//
//==========================================================================
gboolean cb_watch_file (GFileMonitor *monitor, GFile *file, GFile *other_file, GFileMonitorEvent event, gpointer data) {
  if (event != G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT) return FALSE;
  sc_reload(NULL);
  return TRUE;
}



//**************************************************************************
//
// main function
//
//**************************************************************************
int main (int argc, char *argv[]) {
  /* embed */
  Zathura.UI.embed = 0;

  Zathura.Config.config_dir = 0;
  Zathura.Config.data_dir = 0;

  char *config_dir = 0;
  char *data_dir = 0;
  const GOptionEntry entries[] = {
    { "reparent",   'e', 0, G_OPTION_ARG_INT,      &Zathura.UI.embed, "Reparents to window specified by xid", "xid" },
    { "config-dir", 'c', 0, G_OPTION_ARG_FILENAME, &config_dir,       "Path to the config directory",         "path" },
    { "data-dir",   'd', 0, G_OPTION_ARG_FILENAME, &data_dir,         "Path to the data directory",           "path" },
    { NULL }
  };

  GOptionContext *context = g_option_context_new(" [file] [password]");
  g_option_context_add_main_entries(context, entries, NULL);

  GError *error = NULL;
  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    printf("Error parsing command line arguments: %s\n", error->message);
    g_option_context_free(context);
    g_error_free(error);
    return 1;
  }
  g_option_context_free(context);

  if (config_dir) Zathura.Config.config_dir = g_strdup(config_dir);
  if (data_dir) Zathura.Config.data_dir = g_strdup(data_dir);

  g_thread_init(NULL);
  gdk_threads_init();

  gtk_init(&argc, &argv);

  init_zathura();
  init_directories();
  init_keylist();
  read_configuration();
  init_settings();
  init_bookmarks();
  init_look();

  if (argc > 1) {
    char *password = (argc == 3) ? argv[2] : NULL;
    if (strcmp(argv[1], "-") == 0) {
      open_stdin(password);
    } else {
      open_file(argv[1], password);
    }
  }

  switch_view(Zathura.UI.document);
  update_status();

  gtk_widget_show_all(GTK_WIDGET(Zathura.UI.window));
  gtk_widget_grab_focus(GTK_WIDGET(Zathura.UI.view));

  if (!Zathura.Global.show_inputbar) gtk_widget_hide(GTK_WIDGET(Zathura.UI.inputbar));
  if (!Zathura.Global.show_statusbar) gtk_widget_hide(GTK_WIDGET(Zathura.UI.statusbar));

  gdk_threads_enter();
  gtk_main();
  gdk_threads_leave();

  return 0;
}
